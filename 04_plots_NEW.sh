#!/bin/bash

#Isabel Castanho I.Castanho@exeter.ac.uk

# Script to build plots for RNA-seq data from Tg4510 mice, after running DESeq2

# MAKE SURE PLOTS ARE CALLING THE RIGHT VARIABLES! #

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

source("https://bioconductor.org/biocLite.R")

library("DESeq2")
library("RUVSeq")
library("EDASeq")
library("genefilter")
library("dplyr")
library("ggplot2")
library("pheatmap")
library("RColorBrewer")
library("geneplotter")
library("gplots")
library("WGCNA")

#install.packages("pheatmap")
#library("pheatmap")


### Heatmaps - STATISTICS

## Genotype
# coloured by direction of coefficient
textM<-cbind(sig_genotype[,c("FDR_adj_genotype", "FDR_adj_age")], matrix(data=1, ncol=3, nrow=nrow(sig_genotype)), sig_genotype[,"FDR_adj_LRT"], matrix(data=1, ncol=3, nrow=nrow(sig_genotype)))
textM<-as.matrix(textM)
textM<-signif(textM, 3)
textM[which(textM > 0.05)]<-""
dat<-cbind(sig_genotype[,"Genotype_TG_vs_WT"], NA, sig_genotype[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")], NA, 
	(sig_genotype[,c("GenotypeTG.Age_months4", "GenotypeTG.Age_months6", "GenotypeTG.Age_months8")]+sig_genotype[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")])) # we have added the interaction effect to the main age effect, so that the interaction columns represent the age relationship in the transgenics
# colours represent direction of beta coefficient

dat<-as.matrix(dat)
colrange<-as.integer(c(min(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE), max(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE))*50+50) # making zero = white

dendro<-hclust(dist(dat[,-c(2,6)]))

gene_order<-dendro$order

dat<-dat[dendro$order,]
textM<-textM[dendro$order,]

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_genotype_colouredcoefficient.pdf", width=10, height=15)
#sizeGrWindow(10,5)
#par(mfrow = c(1,1))
#par(mar = c(10,5,1,2))
#labeledHeatmap(dat, xLabels = colnames(dat), yLabels = "", colors = redWhiteGreen(100)[colrange[1]:colrange[2]], setStdMargin = FALSE, ySymbols = modSum.sub[,1])
labeledHeatmap(
	Matrix = dat, # matrix
	xLabels = c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG"),
	yLabels = rownames(textM),
	textMatrix = textM,
	cex.text = 0.5,
	cex.lab.y = 0.5, 
	colors = blueWhiteRed(100)[colrange[1]:colrange[2]], 
	setStdMargin = FALSE)
dev.off()

# coloured by FDR corrected p-value
textM <- sig_genotype[,c("FDR_adj_genotype", "FDR_adj_age", "FDR_adj_LRT")]
rownames(textM) <- rownames(sig_genotype)
textM <- textM[gene_order,]
textM<- as.matrix(textM)
textM <- signif(textM, 3)

skewdPcols<-c(colorRampPalette(c("white", "orange"))(10), colorRampPalette(c("orange", "red"))(100))

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_genotype_colouredpvalue.pdf", width=4, height=15)
labeledHeatmap(
	Matrix = -log10(textM), # matrix
	xLabels = c("Genotype", "Age", "Interaction"),
	yLabels = rownames(textM), 
	textMatrix = textM,
	cex.text = 0.5,
	cex.lab.y = 0.5, 
	colors = skewdPcols,
	plotLegend = FALSE,  
	setStdMargin = FALSE,
	main = paste("FDR-corrected p-values"))
dev.off()



## Age
# coloured by direction of coefficient
textM<-cbind(sig_age[,c("FDR_adj_genotype", "FDR_adj_age")], matrix(data=1, ncol=3, nrow=nrow(sig_age)), sig_age[,"FDR_adj_LRT"], matrix(data=1, ncol=3, nrow=nrow(sig_age)))
textM<- as.matrix(textM)
textM <- signif(textM, 3)
textM[which(textM > 0.05)]<-""
dat<-cbind(sig_age[,"Genotype_TG_vs_WT"], NA, sig_age[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")], NA, 
	(sig_age[,c("GenotypeTG.Age_months4", "GenotypeTG.Age_months6", "GenotypeTG.Age_months8")]+sig_age[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")])) # we have added the interaction effect to the main age effect, so that the interaction columns represent the age relationship in the transgenics
# colours represent direction of beta coefficient

dat<- as.matrix(dat)
colrange<-as.integer(c(min(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE), max(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE))*50+50) # making zero = white

dendro<-hclust(dist(dat[,-c(2,6)]))

gene_order<-dendro$order

dat<-dat[dendro$order,]
textM<-textM[dendro$order,]

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_age_colouredcoefficient.pdf", width=10, height=10)
#sizeGrWindow(10,15)
#par(mfrow = c(1,1))
#par(mar = c(10,5,1,2))
#labeledHeatmap(dat, xLabels = colnames(dat), yLabels = "", colors = redWhiteGreen(100)[colrange[1]:colrange[2]], setStdMargin = FALSE, ySymbols = modSum.sub[,1])
labeledHeatmap(
	Matrix = dat,
	xLabels = c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG"),
	yLabels = rownames(textM), 
	textMatrix = textM,
	cex.text = 0.5,
	cex.lab.y = 0.5, 
	colors = blueWhiteRed(100)[colrange[1]:colrange[2]], 
	setStdMargin = FALSE)
dev.off()

# coloured by FDR corrected p-value
textM <- sig_age[,c("FDR_adj_genotype", "FDR_adj_age", "FDR_adj_LRT")]
rownames(textM) <- rownames(sig_age)
textM <- textM[gene_order,]
textM<- as.matrix(textM)
textM <- signif(textM, 3)

skewdPcols<-c(colorRampPalette(c("white", "orange", "red"))(200))

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_age_colouredpvalue.pdf", width=4, height=10)
labeledHeatmap(
	Matrix = -log10(textM), # matrix
	xLabels = c("Genotype", "Age", "Interaction"),
	yLabels = rownames(textM), 
	textMatrix = textM,
	cex.text = 0.5,
	cex.lab.y = 0.5, 
	colors = skewdPcols,
	plotLegend = FALSE,  
	setStdMargin = FALSE,
	main = paste("FDR-corrected p-values"))
dev.off()


## LRT (Interaction)
# coloured by direction of coefficient
textM<-cbind(sig_LRT[,c("FDR_adj_genotype", "FDR_adj_age")], matrix(data=1, ncol=3, nrow=nrow(sig_LRT)), sig_LRT[,"FDR_adj_LRT"], matrix(data=1, ncol=3, nrow=nrow(sig_LRT)))
textM<- as.matrix(textM)
textM <- signif(textM, 3)
textM[which(textM > 0.05)]<-""
dat<-cbind(sig_LRT[,"Genotype_TG_vs_WT"], NA, sig_LRT[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")], NA, 
	(sig_LRT[,c("GenotypeTG.Age_months4", "GenotypeTG.Age_months6", "GenotypeTG.Age_months8")]+sig_LRT[,c("Age_months_4_vs_2", "Age_months_6_vs_2", "Age_months_8_vs_2")])) # we have added the interaction effect to the main age effect, so that the interaction columns represent the age relationship in the transgenics
# colours represent direction of beta coefficient

dat<- as.matrix(dat)
colrange<-as.integer(c(min(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE), max(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE))*50+50) # making zero = white

dendro<-hclust(dist(dat[,-c(2,6)]))

gene_order<-dendro$order

dat<-dat[dendro$order,]
textM<-textM[dendro$order,]

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_interaction_colouredcoefficient.pdf", width=10, height=20)
#sizeGrWindow(10,5)
#par(mfrow = c(1,1))
#par(mar = c(10,5,1,2))
#labeledHeatmap(dat, xLabels = colnames(dat), yLabels = "", colors = redWhiteGreen(100)[colrange[1]:colrange[2]], setStdMargin = FALSE, ySymbols = modSum.sub[,1])
labeledHeatmap(
	Matrix = dat,
	xLabels = c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG"),
	#yLabels = rownames(textM), 
	#textMatrix = textM,
	#cex.text = 0.1,
	#cex.lab.y = 0.1,
	colors = blueWhiteRed(100)[colrange[1]:colrange[2]], 
	setStdMargin = FALSE)
dev.off()

# coloured by FDR corrected p-value
textM <- sig_LRT[,c("FDR_adj_genotype", "FDR_adj_age", "FDR_adj_LRT")]
rownames(textM) <- rownames(sig_LRT)
textM <- textM[gene_order,]
textM<- as.matrix(textM)
textM <- signif(textM, 3)

skewdPcols<-c(colorRampPalette(c("white", "orange"))(10), colorRampPalette(c("orange", "red"))(100))

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/stats_heatmap_interaction_colouredpvalue.pdf", width=4, height=20)
labeledHeatmap(
	Matrix = -log10(textM), # matrix
	xLabels = c("Genotype", "Age", "Interaction"),
	#yLabels = rownames(textM), 
	#textMatrix = textM,
	#cex.text = 0.5,
	#cex.lab.y = 0.5, 
	colors = skewdPcols,
	plotLegend = FALSE,  
	setStdMargin = FALSE,
	main = paste("FDR-corrected p-values"))
dev.off()


### Heatmaps - SAMPLES

# get normalized counts
dds_sizefactors <- estimateSizeFactors(dds)
normalized_counts <- counts(dds_sizefactors, normalized=TRUE)
save(normalized_counts, file="normalized_counts_Tg4510.RData")

# code colours
ann_colors = list(
	Genotype=c(WT="#262626", TG="#73B5BF"),
	Age_months=c("2"="#ff99ff", "4"="#bf00ff", "6"="#8000ff", "8"="#660066"))

## Genotype
# grouped, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_genotype.pdf", width=10, height=15)
pheatmap(
	countdata[rownames(sig_genotype), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	#clustering_distance_rows = "euclidean",
	#clustering_distance_cols = "euclidean",
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	fontsize_row = 5,
	scale="row")
dev.off()

# grouped, normalized counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_genotype_normalized.pdf", width=10, height=15)
pheatmap(
	normalized_counts[rownames(sig_genotype), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	#clustering_distance_rows = "euclidean",
	#clustering_distance_cols = "euclidean",
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	fontsize_row = 5,
	scale="row")
dev.off()

# clustered, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_genotype_clustered.pdf", width=10, height=15)
pheatmap(
	countdata[rownames(sig_genotype), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	fontsize_row = 5,
	scale="row")
dev.off()

# clustered, normalized
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_genotype_clustered_normalized.pdf", width=10, height=15)
pheatmap(
	normalized_counts[rownames(sig_genotype), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	fontsize_row = 5,
	scale="row")
dev.off()


## Age
# grouped, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_age.pdf", width=10, height=10)
pheatmap(
	countdata[rownames(sig_age), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=TRUE,
	scale="row")
dev.off()

# grouped, normalized counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_age_normalized.pdf", width=10, height=10)
pheatmap(
	normalized_counts[rownames(sig_age), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=TRUE,
	scale="row")
dev.off()

# clustered, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_age_clustered.pdf", width=10, height=10)
pheatmap(
	countdata[rownames(sig_age), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=TRUE,
	scale="row")
dev.off()

# clustered, normalized
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_age_clustered_normalized.pdf", width=10, height=10)
pheatmap(
	normalized_counts[rownames(sig_age), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=TRUE,
	scale="row")
dev.off()


## LRT (Interaction)
# grouped, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_interaction.pdf", width=10, height=20)
pheatmap(
	countdata[rownames(sig_LRT), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=FALSE,
	#fontsize_row = 5,
	scale="row")
dev.off()

# grouped, normalized counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_interaction_normalized.pdf", width=10, height=20)
pheatmap(
	normalized_counts[rownames(sig_LRT), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=FALSE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=FALSE,
	#fontsize_row = 5,
	scale="row")
dev.off()

# clustered, raw counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_interaction_clustered.pdf", width=10, height=20)
pheatmap(
	countdata[rownames(sig_LRT), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=FALSE,
	#fontsize_row = 5,
	scale="row")
dev.off()

# clustered, normalized counts
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/samples_heatmap_interaction_clustered_normalized.pdf", width=10, height=20)
pheatmap(
	normalized_counts[rownames(sig_LRT), order(coldata$Genotype, coldata$Age_months)],
	color = blueWhiteRed(100)[colrange[1]:colrange[2]],
	cluster_rows=TRUE,
	cluster_cols=TRUE,
	annotation_col=coldata[ order(coldata$Genotype, coldata$Age_months),c("Genotype", "Age_months")],
	annotation_colors=ann_colors,
	show_colnames=FALSE,
	show_rownames=FALSE,
	#fontsize_row = 5,
	scale="row")
dev.off()