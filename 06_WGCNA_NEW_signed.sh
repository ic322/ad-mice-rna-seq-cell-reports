#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Part 2 of WGCNA RNA-seq Tg4510 mice
# WGCNA RNA-seq Tg4510 mice # SIGNED NETWORK
# R version 3.4.3 (2017-11-30)
# WGCNA 1.63

# Followed these instructions: https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/index.html

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")


## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore")) 
#install.packages("WGCNA")

# Load the WGCNA package
library(WGCNA)
library(RColorBrewer)
library(gplots)
library(ggplot2)
library(reshape2)

options(stringsAsFactors = FALSE); # This setting is important, do not omit.

enableWGCNAThreads(10) # reduce this not to use all of knight


# 2.a.2 One-step network construction and module detection
##SIGNED NETWORK
# Constructing the gene network and identifying modules is now a simple function call [first check how many genes to decide maxBlockSize - nrow(countdata), for example]:
signed_net = blockwiseModules(rlog_counts, power = 10, 
	TOMType = "signed", minModuleSize = 30, maxBlockSize = 25000,
	reassignThreshold = 0, mergeCutHeight = 0.25, 
	numericLabels = TRUE, pamRespectsDendro = FALSE, 
	saveTOMs = TRUE, 
	saveTOMFileBase = "Tg4510_gene_networkTOM_signed", 
	verbose = 3)

save(signed_net, file = "Tg4510_RNAseq_networkConstruction-auto_signed.RData")

# Minimum module size of 30
# medium sensitivity
# (deepSplit=2) to cluster splitting.
# The parameter mergeCutHeight is the threshold for merging of modules. 
# We have also instructed the function to return numeric, rather than color, labels for modules, and to save the Topological Overlap Matrix.
# The output of the function may seem somewhat cryptic, but it is easy to use.
# For example, net$colors contains the module assignment, and net$MEs contains the module eigengenes of the modules.

# A word of caution for the readers who would like to adapt this code for their own data.
# The function blockwiseModules has many parameters, and in this example most of them are left at their default value.
# the parameter maxBlockSize tells the function how large the largest block can be that the reader’s computer can handle.
# The default value is 5000 which is appropriate for most modern desktops.
# Note that if this code were to be used to analyze a data set with more than 5000 probes, the function blockwiseModules will split the data set into several blocks.

# We now return to the network analysis. To see how many modules were identified and what the module sizes are, one can use table(net$colors).
table(signed_net$colors)
#  indicates that there are 17 modules, labeled 1 through 17 in order of descending size, with sizes ranging from 3091 to 33 genes. The label 0 is reserved for genes outside of all modules (5642)

# The hierarchical clustering dendrogram (tree) used for the module identification is returned in net$dendrograms[[1]].
# The dendrogram can be displayed together with the color assignment using the following code:
# Convert labels to colors for plotting
mergedColors_signed = labels2colors(signed_net$colors)
names(mergedColors_signed)<-colnames(rlog_counts)


# 
datTraits2 = datTraits
datTraits2$Genotype <- factor(datTraits2$Genotype, levels = c("WT", "TG"))
datTraits2$Age_months <- factor(datTraits2$Age_months, levels = c("2", "4", "6", "8"))
datTraits2$Group_ID <- factor(datTraits2$Group_ID, levels = c("Tg4510_WT_2m", "Tg4510_WT_4m", "Tg4510_WT_6m", "Tg4510_WT_8m", "Tg4510_TG_2m", "Tg4510_TG_4m", "Tg4510_TG_6m", "Tg4510_TG_8m"))


# Plot the dendrogram and the module colors underneath
pdf(file = "plots/WGCNA_dendogram_modules_signed.pdf", width = 20, height = 9)
plotDendroAndColors(
	signed_net$dendrograms[[1]],
	mergedColors_signed[signed_net$blockGenes[[1]]],
	"Module colors",
	dendroLabels = FALSE,
	addGuide = TRUE,
	guideHang = 0.05)
dev.off()

# number of genes per module
write.csv(table(net$colors), file = "WGCNA_genespermodule.csv") 

moduleLabels_signed = signed_net$colors
moduleColors_signed = labels2colors(signed_net$colors)
MEs_signed = signed_net$MEs;
geneTree_signed = signed_net$dendrograms[[1]];

save(MEs_signed, moduleLabels_signed, moduleColors_signed, geneTree_signed, file = "Tg4510_RNAseq_network_modules_signed.RData")


### 3 Relating modules to traits
## 3.a Quantifying module–trait associations
# In this analysis we would like to identify modules that are significantly associated with the measured clinical traits.
# Since we already have a summary profile (eigengene) for each module, we simply correlate eigengenes with external traits and look for the most significant associations

# Define numbers of genes and samples
nGenes_signed = ncol(rlog_counts);
nSamples_signed = nrow(rlog_counts);
# Recalculate MEs with color labels
MEs0_signed = moduleEigengenes(rlog_counts, moduleColors_signed)$eigengenes
MEs_signed = orderMEs(MEs0_signed)

## Correlations with pathology (burden) [We didn't have pathology data in coldata before, so we used an updated .csv file and created a new variable]
datTraits3 <- read.csv("Tg4510_samples_table.csv", row.names=1, stringsAsFactors=FALSE)
colnames(datTraits3)
rownames(datTraits3)
dim(datTraits3)
head(datTraits3)

identical((datTraits3$Tissue_ID), rownames(rlog_counts))
identical((datTraits3$Tissue_ID), rownames(datExpr))

# remove ECX29 [see 05_WGCNA_NEW.sh]
datTraits3 = datTraits3[-grep("ECX29", datTraits3$Tissue_ID),]; # removed the row correspondent to the sample removed above

identical((datTraits3$Tissue_ID), rownames(rlog_counts))
identical((datTraits3$Tissue_ID), rownames(datExpr))

rownames(datTraits3) = rownames(rlog_counts)
datTraits3 = datTraits3[15:24];
head(datTraits3)

moduleTraitCor = cor(MEs_signed, datTraits3, use = "p");
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples);

# a suitable graphical representation will help in reading the table. We color code each association by the correlation value:
# Will display correlations and their p-values
textMatrix = paste(signif(moduleTraitCor, 2), "\n(",
signif(moduleTraitPvalue, 1), ")", sep = "");
dim(textMatrix) = dim(moduleTraitCor)

# Display the correlation values within a heatmap plot
pdf(file = "plots/WGCNA_correlations_tau_pathology.pdf", width = 10, height = 10)
#dev.new(width=10, height=10)
par(mar = c(8, 8, 3, 1)); # bottom, left, top, and right
labeledHeatmap(Matrix = moduleTraitCor,
	xLabels = c("Hippocampus", "CA1", "CA3", "DG", "Whole cortex", "M2", "M1", "Visual cortex", "Retrosplenial cortex", "Thalamus"),
	yLabels = names(MEs_signed),
	ySymbols = names(MEs_signed),
	colorLabels = FALSE,
	colors = blueWhiteRed(50),
	textMatrix = textMatrix,
	setStdMargins = FALSE,
	cex.text = 1,
	zlim = c(-1,1),
	main = "Module-trait relationships with tau pathology")
dev.off()
# Module-trait associations. Each row corresponds to a module eigengene, column to a trait. Each cell contains the corresponding correlation and p-value.
# The table is color-coded by correlation according to the color legend.


## Gene relationship to trait and important modules: Gene Significance and Module Membership
# We quantify associations of individual genes with our trait of interest (pathology in the hippocampus) by defining Gene Significance GS as (the absolute value of) the correlation between the gene and the trait.
# For each module, we also define a quantitative measure of module membership MM as the correlation of the module eigengene and the gene expression profile.
# This allows us to quantify the similarity of all genes on the array to every module.

## Hippocampus
# Define variable HIP containing the Pathology_HIP column of datTraits3
HIP = as.data.frame(datTraits3$Pathology_HIP);
names(HIP) = "Hippocampus"
# names (colors) of the modules
modNames = substring(names(MEs_signed), 3)

geneModuleMembership = as.data.frame(cor(rlog_counts, MEs_signed, use = "p"));
MMPvalue = as.data.frame(corPvalueStudent(as.matrix(geneModuleMembership), nSamples));

names(geneModuleMembership) = paste("MM", modNames, sep="");
names(MMPvalue) = paste("p.MM", modNames, sep="");

geneTraitSignificance = as.data.frame(cor(rlog_counts, HIP, use = "p"));
GSPvalue = as.data.frame(corPvalueStudent(as.matrix(geneTraitSignificance), nSamples));

names(geneTraitSignificance) = paste("GS.", names(HIP), sep="");
names(GSPvalue) = paste("p.GS.", names(HIP), sep="");

# Black module
pdf(file = "plots/WGCNA_MM_GS_HIP_black.pdf")
module = "black"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Salmon module
pdf(file = "plots/WGCNA_MM_GS_HIP_salmon.pdf")
module = "salmon"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Turquoise module
pdf(file = "plots/WGCNA_MM_GS_HIP_turquoise.pdf")
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Grey60 module
pdf(file = "plots/WGCNA_MM_GS_HIP_grey60.pdf")
module = "grey60"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Green module
pdf(file = "plots/WGCNA_MM_GS_HIP_green.pdf")
module = "green"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Purple module
pdf(file = "plots/WGCNA_MM_GS_HIP_purple.pdf")
module = "purple"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Blue module
pdf(file = "plots/WGCNA_MM_GS_HIP_blue.pdf")
module = "blue"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Yellow module
pdf(file = "plots/WGCNA_MM_GS_HIP_yellow.pdf")
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Light cyan module
pdf(file = "plots/WGCNA_MM_GS_HIP_lightcyan.pdf")
module = "lightcyan"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()

# Red module
pdf(file = "plots/WGCNA_MM_GS_HIP_red.pdf")
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	xlab = paste("Module Membership in", module, "module"),
	ylab = "Gene significance for hippocampus",
	main = paste("Module membership vs. gene significance\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg = module, pch = 21)
dev.off()




## STATISTICS

# number of modules -> important for downstream code
ncol(MEs_signed) # 18 modules for Tg4510

## Statistics and heatmap coloured by p-value

# nrow = # of modules
moduleP_signed <- matrix(data=NA, ncol=3, nrow=18)

colnames(moduleP_signed) <- c("Genotype", "Age", "Interaction")

for (i in 1:ncol(MEs_signed)) {
	H1 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months + datTraits2$Genotype *datTraits2$Age_months)
	H0 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months)
	H0_2 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype)
	moduleP_signed[i,1] <- summary(H0)$coefficients[2,4] # genotype effect
	moduleP_signed[i,2] <- anova(H0,H0_2)[2,6] # age
	moduleP_signed[i,3] <- anova(H0,H1)[2,6] # interaction effect
}

rownames(moduleP_signed) <- colnames(MEs_signed)

textmatrix <- signif(moduleP_signed, 3) # signif() - function to round numbers
textmatrix2 <- matrix(data="", ncol=ncol(textmatrix), nrow=nrow(textmatrix))
#textmatrix2[which(textmatrix<0.05)] <- "*" # corrected for the number of modules (18) is textmatrix2[which(textmatrix<0.05/18)] (multiple comparisons!)
textmatrix2[which(textmatrix<0.05/18)] <- "*" # corrected for the number of modules (18) is textmatrix2[which(textmatrix<0.05/18)] (multiple comparisons!)
textmatrix3 <- matrix(data=paste(textmatrix, textmatrix2, sep=""), ncol=ncol(textmatrix), nrow=nrow(textmatrix), byrow=FALSE)

skewdPcols<-c(colorRampPalette(c("white", "orange", "red"))(10))

pdf(file = "plots/WGCNA_heatmap_pvalues_signed.pdf", width = 6, height = 12)
par(mar=c(6,10,3,2)) # c(bottom, left, top, right)
labeledHeatmap(
	Matrix = -log10(moduleP_signed),
	xLabels = colnames(moduleP_signed), 
	yLabels = colnames(MEs_signed), #names(MEs)[-ncol(MEs)]
	ySymbols = colnames(MEs_signed), #names(MEs)[-ncol(MEs)]
	colorLabels = TRUE, 
	colors = skewdPcols, 
	setStdMargins = FALSE, 
	cex.text = 1,
	textMatrix = textmatrix3,
	plotLegend = FALSE, 
	main = paste("Module-trait relationships"))
dev.off()


## Statistics and heatmap coloured by direction of coefficient
# nrow = # of modules
moduleP <- matrix(data=NA, ncol=9, nrow=18)
dat <- matrix(data=NA, ncol=9, nrow=18)

colnames(moduleP) <- c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG")
rownames(moduleP) <- colnames(MEs_signed)
colnames(dat) <- c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG")
rownames(dat) <- colnames(MEs_signed)

for (i in 1:ncol(MEs_signed)) {
	H1 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months + datTraits2$Genotype *datTraits2$Age_months)
	H0 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months)
	H0_2 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype)
	moduleP[i,1] <- summary(H0)$coefficients[2,4] # genotype effect
	moduleP[i,2] <- anova(H0,H0_2)[2,6] #age
	moduleP[i,6] <- anova(H0,H1)[2,6] # interaction effect
	dat[i,1] <- coef(H0)[2] # genotype effect
	dat[i,3] <- coef(H0)[3] # 4m compared to 2m
	dat[i,4] <- coef(H0)[4] # 6m compared to 2m
	dat[i,5] <- coef(H0)[5] # 8m compared to 2m
	dat[i,7] <- coef(H1)[6] # interaction 4m TG compared to 2m
	dat[i,8] <- coef(H1)[7] # interaction 6m TG compared to 2m 
	dat[i,9] <- coef(H1)[8] # interaction 8m TG compared to 2m
}

textmatrix <- cbind(moduleP[,c("Genotype", "Age")], matrix(data=1, ncol=3, nrow=nrow(moduleP)), moduleP[,"Interaction"], matrix(data=1, ncol=3, nrow=nrow(moduleP)))
colnames(textmatrix) <- c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG")
textmatrix<-as.matrix(textmatrix)

textmatrix2 <- signif(textmatrix, 3) # signif() - function to round numbers
textmatrix2[which(textmatrix > 0.05)]<-""

textmatrix3 <- matrix(data="", ncol=ncol(textmatrix), nrow=nrow(textmatrix))
#textmatrix3[which(textmatrix<0.05)] <- "*" # corrected for the number of modules (18) is textmatrix2[which(textmatrix<0.05/18)] (multiple comparisons!)
textmatrix3[which(textmatrix<0.05/18)] <- "*" # corrected for the number of modules (18) is textmatrix2[which(textmatrix<0.05/18)] (multiple comparisons!)
textmatrix3 <- matrix(data=paste(textmatrix2, textmatrix3, sep=""), ncol=ncol(textmatrix), nrow=nrow(textmatrix), byrow=FALSE)

colrange<-as.integer(c(min(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE), max(dat, na.rm = TRUE)/max(abs(dat), na.rm = TRUE))*50+50) # making zero = white

pdf(file = "plots/WGCNA_heatmap_colouredcoefficient_signed.pdf", width = 12, height = 12)
par(mar=c(6,10,3,2)) # c(bottom, left, top, right)
labeledHeatmap(
	Matrix = dat,
	xLabels = c("Genotype", "Age", "4 months", "6 months", "8 months", "Interaction", "4 months TG", "6 months TG", "8 months TG"), 
	yLabels = colnames(MEs_signed), #names(MEs)[-ncol(MEs)]
	ySymbols = rownames(dat), #names(MEs)[-ncol(MEs)]
	colorLabels = TRUE, 
	colors = blueWhiteRed(100)[colrange[1]:colrange[2]], 
	setStdMargins = FALSE, 
	cex.text = 1,
	textMatrix = textmatrix3, 
	main = paste("Module-trait relationships"))
dev.off()

### New Table (stat value and p-value)
# nrow = # of modules
table_publication <- matrix(data=NA, ncol=9, nrow=18)

colnames(table_publication) <- c("Genotype t", "Genotype coef", "Genotype p", "Age F", "Age coef", "Age p", "Interaction F", "Interaction coef", "Interaction p")
rownames(table_publication) <- colnames(MEs_signed)

for (i in 1:ncol(MEs_signed)) {
	H1 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months + datTraits2$Genotype *datTraits2$Age_months)
	H0 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype + datTraits2$Age_months)
	H0_2 <- lm(as.numeric(MEs_signed[,i]) ~ datTraits2$Genotype)
	table_publication[i,1] <- summary(H0)$coefficients[2,3] # genotype effect (t-value)
	table_publication[i,2] <- coef(H0)[2] # genotype effect (coef)
	table_publication[i,3] <- summary(H0)$coefficients[2,4] # genotype effect (p-value)
	table_publication[i,4] <- anova(H0,H0_2)[2,5] #age (F-value)
	table_publication[i,5] <- coef(H0)[5] # 8m compared to 2m
	table_publication[i,6] <- anova(H0,H0_2)[2,6] #age (p-value)
	table_publication[i,7] <- anova(H0,H1)[2,5] # interaction effect (F-value)
	table_publication[i,8] <- coef(H1)[8] # interaction 8m TG compared to 2m
	table_publication[i,9] <- anova(H0,H1)[2,6] # interaction effect (p-value)
}

write.csv(table_publication, file = "Tg4510_WGCNA_STATS.csv")