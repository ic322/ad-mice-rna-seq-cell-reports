#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Functional annotation and gene ontology | goseq WGCNA Tg4510 mice

# Following 06_WGCNA_MEW_signed.sh

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite("BiocUpgrade") # goseq was not working properly because of the version of bioconductor ### Still didn't solve it, but just for function supportedOrganisms()

library(DESeq2) # DESeq2_1.16.1
library(GO.db)
library(goseq) # goseq_1.30.0 
library (org.Mm.eg.db)
library(annotate)
library(geneLenDataBase)

### WGCNA data
#install.packages("WGCNA") # if necessary
library(WGCNA)
options(stringsAsFactors = FALSE); # This setting is important, do not omit.
enableWGCNAThreads(10) # reduce this not to use all of knight
# Our variables:
#datExpr
#datTraits2
#names(signed_net)
#moduleLabels_signed = signed_net$colors # (moduleLabels)
#moduleColors_signed = labels2colors(signed_net$colors) # (moduleColors)
#MEs_signed = signed_net$MEs; # (consMEs)
#geneTree_signed = signed_net$dendrograms[[1]]; # (consTree)
#nGenes_signed = ncol(rlog_counts);
#nSamples_signed = nrow(rlog_counts);

# Constructing a gene vector for "goseq"
assayed_genes <- rownames(geneInfoALL)
length(assayed_genes)

## goseq for each WGCNA module 

uniqueColors <- unique(geneInfoALL$moduleColor)

for (i in 1:length(uniqueColors)) {

	colorString <- uniqueColors[i]
	color <- which(geneInfoALL$moduleColor==colorString)
	geneInfoALL_color <- geneInfoALL[color,]
	#nrow(geneInfoALL_color)
	#rownames(geneInfoALL_color)

	## Color module
	de_genes <- rownames(geneInfoALL_color)
	#length(de_genes)
	genes_vector=as.integer(assayed_genes%in%de_genes)
	names(genes_vector)=assayed_genes
	table(genes_vector)

	# Fitting the Probability Weighting Function (PWF)
	pwf=nullp(genes_vector,"mm10","geneSymbol")
	head(pwf)

	pdf(file = paste("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/goseq/WGCNA_pwf_Tg4510_", colorString, ".pdf", sep=""))
	plotPWF(pwf)
	dev.off()

	# Using the Wallenius approximation
	GO_wall=goseq(pwf,"mm10","geneSymbol")
	# For 2543 genes, we could not find any categories. These genes will be excluded.
	head(GO_wall)
	# The resulting object is ordered by GO category over representation amongst DE genes.

	# To identify categories significantly enriched/unenriched below some p-value cutoff, it is necessary to first apply some kind of multiple hypothesis testing correction.
	# For example, GO categories over enriched using a .05 FDR cutoff [Benjamini and Hochberg, 1995] are:
	enriched_GO=GO_wall$category[p.adjust(GO_wall$over_represented_pvalue, method="BH")<.05]
	#head(enriched_GO)
	length(enriched_GO)

	BH_adj_GO <- p.adjust(GO_wall[,"over_represented_pvalue"], method="BH")
	class(BH_adj_GO)
	enriched_GO <- cbind(GO_wall, BH_adj_GO)
	class(enriched_GO)
	dim(enriched_GO)
	head(enriched_GO)
	enriched_GO <- enriched_GO[which(enriched_GO$BH_adj_GO<0.05),]
	class(enriched_GO)
	dim(enriched_GO)
	head(enriched_GO)

	write.csv(enriched_GO, file = paste("goseq/goseq_WGCNA_", colorString, ".csv", sep=""))

}

sessionInfo()



