#!/bin/bash

# Isabel Castanho (I.S.Castanho@exeter.ac.uk)

# Script to count reads from Tg4510 mice using featureCounts
# http://bioinf.wehi.edu.au/featureCounts/
# featureCounts seems to be better for paired-end reads since HTSeq seems to be too conservative: http://bioinformatics.cvr.ac.uk/blog/featurecounts-or-htseq-count/

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_bam/

### removed L19 & P22 (low reads or contamination)

# Run featureCounts # featureCounts v1.5.2
featureCounts -p -a /mnt/data1/isabel/reference/genes.gtf -o counts.summary /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_bam/*.bam
# -p: If specified, fragments (or templates) will be counted instead of reads. This option is only applicable for paired-end reads.

# The output of featureCounts program includes a count table (counts.summary) and a summary of counting results (counts.summary.summary.