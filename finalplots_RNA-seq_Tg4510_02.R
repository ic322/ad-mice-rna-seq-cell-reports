#Isabel Castanho I.S.Castanho@exeter.ac.uk

# FINAL PLOTS FOR PAPER - Tg4510 mice # Part 2 # REVISION

##### Tg4510 #####

# setwd
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

library("DESeq2")
library("pheatmap")
library("WGCNA")
library(vioplot)

color_Tg4510_TG <- "#00AEC9"

####################################################################### Expression plots #######################################################################

# Normalized counts individual genes
#dds_sizefactors <- estimateSizeFactors(dds)
#normalized_counts <- counts(dds_sizefactors, normalized=TRUE)

data <- as.data.frame(normalized_counts)

genes_to_plot <- c("Syt1", "Eno2", "Snap25", "Dlg4", "Syp",
	"Aif1", "Tmem119", "Slc2a5", "Fcrls", "Cd68",
	"Gfap", "Sox9", "S100b", "Aqp4",
	"Olig2", "Sox10", "Mag")

for(index in 1:length(genes_to_plot)) {
	gene <- genes_to_plot[index]
	data_plot <- cbind(coldata[,c("Age_months","Genotype")], t(data[gene,]))
	means_WT <- c()
	means_TG <- c()
	for (age in c("2","4","6","8")) {
		means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,gene]))
		means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,gene]))
	}
	pdf(file = paste("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures_part2/Tg4510/", gene, "_", ".pdf", sep=""), width=5, height=10)
	#dev.new(width=5, height=10)
	par(mar=c(5,5,5,3))
	plot(y=data[gene,], x=coldata$Age_months,
		col = c("black", color_Tg4510_TG)[coldata$Genotype],
		pch = 19,
		cex=2,
		xlim=c(0.5,4.5),
		ylim=c(min(data[gene,]), max(data[gene,])),
		axes=FALSE, ann=FALSE)
	axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
	axis(2, cex.axis=2)
	title(main = gene, xlab = "Age (months)", ylab = "Normalised counts", cex.main=2, cex.lab=2)
	lines(means_WT, lty=2, col="black", lwd=3)
	lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
	dev.off()
}


####################################################################### Neuropathology ECX #######################################################################

pat_data <- read.csv("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510_pathology_ECX.csv", stringsAsFactors=FALSE)
colnames(pat_data)
rownames(pat_data)

pat_data$Age_months <- as.factor(pat_data$Age_months)
pat_data$Genotype <- as.factor(pat_data$Genotype)
pat_data$Genotype <- relevel(pat_data$Genotype, "WT")
levels(pat_data$Genotype)

# plot
region <- "ECX"
data_plot <- pat_data
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,region], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,region], na.rm=TRUE))
}
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/tau_pathology_Tg4510_ECX.pdf", width=5, height=10)
#dev.new(width=5, height=10)
par(mar=c(5,5,5,3))
plot(y=data_plot[,region], x=as.numeric(data_plot$Age_months),
	col = c("black", color_Tg4510_TG)[data_plot$Genotype],
	pch = 19,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(0, 50),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main = "Entorhinal cortex", xlab = "Age (months)", ylab = "Immunoreactivity (% burden)", cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

# ylim=c(0, 30)
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/tau_pathology_Tg4510_ECX_30.pdf", width=5, height=10)
#dev.new(width=5, height=10)
par(mar=c(5,5,5,3))
plot(y=data_plot[,region], x=as.numeric(data_plot$Age_months),
	col = c("black", color_Tg4510_TG)[data_plot$Genotype],
	pch = 19,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(0, 30),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main = "Entorhinal cortex", xlab = "Age (months)", ylab = "Immunoreactivity (% burden)", cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

### Two-way ANOVA with interaction effect
res.aov3 <- aov(ECX ~ Genotype * Age_months, data = pat_data)
summary(res.aov3)

## Correlation plots ECX - HIP
data_plot <- read.csv("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510_pathology_ECX_HIP.csv", stringsAsFactors=FALSE)
colnames(data_plot)
rownames(data_plot)

data_plot$Age_months <- as.factor(data_plot$Age_months)
data_plot$Genotype <- as.factor(data_plot$Genotype)
data_plot$Genotype <- relevel(data_plot$Genotype, "WT")
levels(data_plot$Genotype)

pdf(file ="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/scatterplot_tau_ECX-HIP.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(data_plot$HIP,
	data_plot$ECX,
	col = c("black", color_Tg4510_TG)[data_plot$Genotype],
	bg=c("white", color_Tg4510_TG),
	pch = 19,
	cex=2,
	axes=FALSE,
	xlab = "Tau pathology in hippocampus",
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(0,50),
	ylim=c(0,50),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()

# ylim=c(0,30)
pdf(file ="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/scatterplot_tau_ECX-HIP_30.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(data_plot$HIP,
	data_plot$ECX,
	col = c("black", color_Tg4510_TG)[data_plot$Genotype],
	bg=c("white", color_Tg4510_TG),
	pch = 19,
	cex=2,
	axes=FALSE,
	xlab = "Tau pathology in hippocampus",
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(0,50),
	ylim=c(0,30),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()


####################################################################### DESeq2 expression - pathology plots #######################################################################

# get DEGs

data <- as.data.frame(normalized_counts)

# column data / phenotypes
coldata <- read.csv("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/Tg4510_coldata_DESeq2Pathology.csv", row.names=1, stringsAsFactors=FALSE)
colnames(coldata)
rownames(coldata)

coldata$Age_months <- as.factor(coldata$Age_months)
coldata$Genotype <- as.factor(coldata$Genotype)
coldata$Genotype <- relevel(coldata$Genotype, "WT")
levels(coldata$Genotype)

data <- data[,rownames(coldata)]

## scatterplots HIPPOCAMPUS
genes_to_plot <- c("C3", "C4b", "Hrsp12", "H2-D1")

for(index in 1:length(genes_to_plot)) {
	gene <- genes_to_plot[index]
	data_plot <- cbind(coldata[,c("Age_months","Genotype", "HIP")], t(data[gene,]))
	pdf(file = paste("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/", gene, "_pathology_HIP.pdf"), width=10, height=10)
	#dev.new(width=10, height=10)
	par(mar=c(9,9,5,3), las=1,bty="l") # bottom, left, top, and right
	verboseScatterplot(data_plot$HIP,
		data_plot[,gene],
		col = c("black", color_Tg4510_TG)[coldata$Genotype],
		bg=c("white", color_Tg4510_TG),
		pch = 19,
		cex=2,
		axes=FALSE,
		xlab = "Tau pathology in hippocampus",
		ylab = "Normalised counts",
		main = gene,
		xlim=c(0,50),
		ylim=c(min(data[gene,]), max(data[gene,])),
		mgp=c(6,1,0), cex.main=2, cex.lab=2)
	axis(side=1, cex.axis=2)
	axis(2, cex.axis=2)
	dev.off()
}

## scatterplots ENTORHINAL CORTEX
genes_to_plot <- c("C4b", "Gfap", "Itgax", "Slc14a1", "Cd68",
	"Mpeg1", "Cd84", "Osmr", "Cst7", "Ctse")

for(index in 1:length(genes_to_plot)) {
	gene <- genes_to_plot[index]
	data_plot <- cbind(coldata[,c("Age_months","Genotype", "ECX")], t(data[gene,]))
	pdf(file = paste("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/", gene, "_pathology_ECX.pdf"), width=10, height=10)
	#dev.new(width=10, height=10)
	par(mar=c(9,9,5,3), las=1,bty="l") # bottom, left, top, and right
	verboseScatterplot(data_plot$ECX,
		data_plot[,gene],
		col = c("black", color_Tg4510_TG)[coldata$Genotype],
		bg=c("white", color_Tg4510_TG),
		pch = 19,
		cex=2,
		axes=FALSE,
		xlab = "Tau pathology in entorhinal cortex",
		ylab = "Normalised counts",
		main = gene,
		xlim=c(0,50),
		ylim=c(min(data[gene,]), max(data[gene,])),
		mgp=c(6,1,0), cex.main=2, cex.lab=2)
	axis(side=1, cex.axis=2)
	axis(2, cex.axis=2)
	dev.off()
}


####################################################################### WGCNA plots #######################################################################
pat_data <- read.csv("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/TTg4510_coldata_WGCNA_ECX.csv", stringsAsFactors=FALSE)
colnames(pat_data)
rownames(pat_data)

pat_data$Age_months <- as.factor(pat_data$Age_months)
pat_data$Genotype <- as.factor(pat_data$Genotype)
pat_data$Genotype <- relevel(pat_data$Genotype, "WT")
levels(pat_data$Genotype)

data <- as.data.frame(t(pat_data[,6:15]))
colnames(data) <- rownames(pat_data)

### Module Eigengenes
data <- as.data.frame(t(blockwiseMEs))
colnames(data) <- rownames(blockwiseMEs)

datTraits2$Age_months <- as.factor(datTraits2$Age_months)
datTraits2$Genotype <- as.factor(datTraits2$Genotype)
datTraits2$Genotype <- relevel(datTraits2$Genotype, "WT")
levels(pat_data$Genotype)


### SCATTERPLOTS
## Scatter plots eigengenes ~ pathology
## TURQUOISE module
# Entorhinal cortex
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_ME_pathology_ECX_turquoise.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(MEs_signed[,"MEturquoise"],
	datTraits3$ECX,
	col = "black",
	bg=c("white", module)[datTraits2$Genotype],
	pch = 21,
	cex=3,
	axes=FALSE,
	xlab = "Module eigengene",
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()

## YELLOW module
# Entorhinal cortex
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_ME_pathology_ECX_yellow.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(MEs_signed[,"MEyellow"],
	datTraits3$ECX,
	col = "black",
	bg=c("white", module)[datTraits2$Genotype],
	pch = 21,
	cex=3,
	axes=FALSE,
	xlab = "Module eigengene",
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()

## RED module
# Entorhinal cortex
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_ME_pathology_ECX_red.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(MEs_signed[,"MEred"],
	datTraits3$ECX,
	col = "black",
	bg=c("white", module)[datTraits2$Genotype],
	pch = 21,
	cex=3,
	axes=FALSE,
	xlab = "Module eigengene",
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()


## Scatter plots Module membership ~ gene significance

# Define variable ECX containing the ECX column of datTraits3
ECX = as.data.frame(datTraits3$ECX);
names(ECX) = "Entorhinal cortex"
# names (colors) of the modules
modNames = substring(names(MEs_signed), 3)

geneModuleMembership = as.data.frame(cor(rlog_counts, MEs_signed, use = "p"));
MMPvalue = as.data.frame(corPvalueStudent(as.matrix(geneModuleMembership), nSamples));

names(geneModuleMembership) = paste("MM", modNames, sep="");
names(MMPvalue) = paste("p.MM", modNames, sep="");

geneTraitSignificance = as.data.frame(cor(rlog_counts, ECX, use = "p"));
GSPvalue = as.data.frame(corPvalueStudent(as.matrix(geneTraitSignificance), nSamples));

names(geneTraitSignificance) = paste("GS.", names(ECX), sep="");
names(GSPvalue) = paste("p.GS.", names(ECX), sep="");

# Turquoise
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_MM_GS_ECX_turquoise.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	col = "black", bg = module, pch = 21,
	axes=FALSE, xlab = paste("Module Membership"), ylab = "Gene significance for entorhinal cortex",
	xlim=c(0.2,1),
	ylim=c(0,1),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()

# Yellow
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_MM_GS_ECX_yellow.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	col = "black", bg = module, pch = 21,
	axes=FALSE, xlab = paste("Module Membership"), ylab = "Gene significance for entorhinal cortex",
	xlim=c(0.2,1),
	ylim=c(0,1),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()

# Red
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures/Tg4510/WGCNA_MM_GS_ECX_red.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
	abs(geneTraitSignificance[moduleGenes, 1]),
	col = "black", bg = module, pch = 21,
	axes=FALSE, xlab = paste("Module Membership"), ylab = "Gene significance for entorhinal cortex",
	xlim=c(0.2,1),
	ylim=c(0,1),
	mgp=c(4,1,0), cex.main=2, cex.lab=2)
axis(side=1, cex.axis=2)
axis(2, cex.axis=2)
dev.off()


## Intramodular connectivity
# We begin by calculating the intramodular connectivity for each gene. ((In network literature, connectivity is often referred to as "degree".)
# The function intramodularConnectivity computes the whole network connectivity kTotal, the within module connectivity kWithin, kOut=kTotal-kWithin, and kDiff=kIn-kOut=2*kIN-kTotal
colorh1 = moduleColors_signed
ADJ1 = abs(cor(rlog_counts,use="p"))^6
Alldegrees1 = intramodularConnectivity(ADJ1, colorh1) # intramodularConnectivity(adjMat, colors, scaleByMax = FALSE)
head(Alldegrees1)

y=datTraits3$ECX
GS1=as.numeric(cor(y,rlog_counts, use="p"))
GeneSignificance=abs(GS1)

# Next module significance is defined as average gene significance.
ModuleSignificance=tapply(GeneSignificance, colorh1, mean, na.rm=T)
# To plot module signicance,
sizeGrWindow(8,7)
par(mfrow = c(1,1))
plotModuleSignificance(GeneSignificance,colorh1)

## Relationship between gene significance and intramodular connectivity
# Plot the gene signicance against intramodular connectivity:
colorlevels = unique(colorh1)
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/final_figures_part2/Tg4510/WGCNA_GS_connectivity_ECX.pdf", width=10, height=10)
for (i in c(1:length(colorlevels))){
	whichmodule=colorlevels[[i]];
	restrict1 = (colorh1==whichmodule);
	par(mar=c(6,6,5,3), las=1,bty="l") # bottom, left, top, and right
	verboseScatterplot(Alldegrees1$kWithin[restrict1],
		GeneSignificance[restrict1],
		main=whichmodule,
		xlab = "Connectivity", ylab = "Gene Significance for entorhinal cortex",
		#abline = TRUE,
		col = "black", bg = colorh1[restrict1], pch = 21,
		axes=FALSE,
		ylim=c(0,1),
		mgp=c(4,1,0), cex.main=2, cex.lab=2)
	axis(side=1, cex.axis=2)
	axis(2, cex.axis=2)
}
dev.off()