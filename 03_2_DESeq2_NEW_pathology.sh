#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# part 2 of 03_DESeq2_NEW (see for more details)
# Script to look for differential expression due to *pathology* for Tg4510 mice 
#### Version: DESeq2_1.16.1

# Differential expression analysis based on the Negative Binomial (a.k.a. Gamma-Poisson) distribution
# The package DESeq2 provides methods to test for differential expression by use of negative binomial generalized linear models

# This function performs a default analysis through the steps:
# 1. estimation of size factors: estimateSizeFactors
# 2. estimation of dispersion: estimateDispersions
# 3. Negative Binomial GLM fitting and Wald statistics: nbinomWaldTest (test = "Wald" -> default)

# After the DESeq function returns a DESeqDataSet object, results tables (log2 fold changes and p-values) can be generated using the results function.

# LRT option (test = "LRT"): tests for significance of change in deviance between a full and reduced model, which are provided as formula. Fitting uses previously calculated sizeFactors 

# See: http://www.bioconductor.org/help/workflows/rnaseqGene/#starting-from-count-matrices

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

source("https://bioconductor.org/biocLite.R")
#biocLite("RUVSeq", lib="/home/epm_ug/em12/jffv201")
#biocLite("EDASeq", lib="/home/epm_ug/em12/jffv201")
#biocLite("DESeq2", lib="/home/epm_ug/em12/jffv201")
#biocLite("tximport", lib="/home/epm_ug/em12/jffv201")

# sessionInfo()

library("DESeq2")
library("RUVSeq")
library("EDASeq")
library("genefilter")
library("dplyr")
library("ggplot2")
library("pheatmap")
library("RColorBrewer")
biocLite("geneplotter")

# column data / phenotypes
coldata <- read.csv("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/Tg4510_coldata_DESeq2Pathology.csv", row.names=1, stringsAsFactors=FALSE)
colnames(coldata)
rownames(coldata)

coldata$Age_months <- as.factor(coldata$Age_months)
coldata$Genotype <- as.factor(coldata$Genotype)
coldata$Genotype <- relevel(coldata$Genotype, "WT")
levels(coldata$Genotype)

#countdata # from featurecounts

# Pathology as continuous variable

## HIPPOCAMPUS
class(coldata$HIP) # numeric

# Histograms - distribution of the data
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/histogram_Pathology_HIP.pdf")
hist(coldata$HIP)
dev.off()

# Remove samples that have NA for pathology (no samples were removed)
coldata_HIP <- coldata[,c("Genotype", "Age_months", "Histology_no", "HIP")]
coldata_HIP_clean <- na.omit(coldata_HIP)

countdata_pathology <- countdata[,rownames(coldata_HIP_clean)]

# design:
dds <- DESeqDataSetFromMatrix(countData = countdata_pathology, colData = coldata_HIP_clean, design = ~HIP)

# Pre-filtering the data set
# Remove the rows that have no or nearly no information about the amount of gene expression
# Removing rows of the DESeqDataSet that have no counts, or only a single count across all samples
nrow(dds)
dds <- dds[ rowSums(counts(dds)) > 6, ] # filtering for non-expressed and lowly expressed genes (sum of expression accross all samples at least 6 because final n = 6-8 mice per group)
nrow(dds)


##### Differential expression analysis
### WALD TEST ###
dds_Wald <- DESeq(dds, test="Wald")
# The Wald test (also called the Wald Chi-Squared Test) is a way to find out if explanatory variables in a model are significant.
# “Significant” means that they add something to the model; variables that add nothing can be deleted without affecting the model in any meaningful way.
# The test can be used for a multitude of different models including those with binary variables or continuous variables.
# The Wald test is a rough approximation of the Likelihood Ratio Test. However, you can run it with a single model (the LR test requires at least two).
# It is also more broadly applicable than the LRT: often, you can run a Wald in situations where no other test can be run.
# For large values of n, the Wald test is roughly equivalent to the t-test; both tests will reject the same values for large sample sizes.
# The Wald, LRT and Lagrange multiplier tests are all equivalent as sample sizes approach infinity (called “asymptotically equivalent”).
# However, samples of a finite size, especially smaller samples, are likely to give very different results.

dds_Wald # class: DESeqDataSet

resultsNames(dds_Wald)

# Building the results table
# Calling results without any arguments will extract the estimated log2 fold changes and p values for the last variable in the design formula
res_Wald <- results(dds_Wald)
res_Wald
results_Wald_frame <- data.frame(res_Wald, stringsAsFactors=FALSE)
write.csv(results_Wald_frame, file = "Tg4510_res_Wald_PATHOLOGY_HIP.csv") 

stats_Wald <- mcols(dds_Wald)
rownames(stats_Wald) <- rownames(dds_Wald)
head(stats_Wald)
stats_Wald_frame <- data.frame(stats_Wald, stringsAsFactors=FALSE)

# DESeq preserves the original counts in counts(dds) saving the replacement counts as a matrix named replaceCounts in assays(dds).
#replacedcounts_Wald <- assays(dds_Wald)[["replaceCounts"]] # 
#head(replacedcounts_Wald) # class(): matrix

save(dds_Wald, res_Wald, stats_Wald, file="DEseq2_Wald_test_results_Tg4510_PATHOLOGY_HIP.RData")

# Stats Tables (final)
FDR_adj_pathology <- p.adjust(stats_Wald[,"WaldPvalue_HIP"], method = "fdr") # we have calculated the FDR-adj pvalue ourselves because 1) they were not available for all analysis,and 2) from DESeq some were "NA"

stats_table <-cbind(FDR_adj_pathology,
	as.data.frame(stats_Wald[,c("WaldPvalue_HIP", "HIP")]))

sig_PATHOLOGY <- stats_table[which(stats_table[,"FDR_adj_pathology"]<0.05),]
write.csv(sig_PATHOLOGY, file = "Tg4510_sig_PATHOLOGY_HIP.csv")

table_publication_PATHOLOGY <- cbind(FDR_adj_pathology,
	as.data.frame(stats_Wald[,c("WaldPvalue_HIP", "HIP", "WaldStatistic_HIP")]))
dim(table_publication_PATHOLOGY)
rownames(table_publication_PATHOLOGY) <- rownames(res_Wald)
head(table_publication_PATHOLOGY)

genes_Tg4510_PATHOLOGY <- rownames(sig_PATHOLOGY)
length(genes_Tg4510_PATHOLOGY)

table_publication_PATHOLOGY <- table_publication_PATHOLOGY[genes_Tg4510_PATHOLOGY,]
head(table_publication_PATHOLOGY)
write.csv(table_publication_PATHOLOGY, file = "Tg4510_sig_PATHOLOGY_HIP_STATS.csv")



## ENTORHINAL CORTEX
class(coldata$ECX) # numeric

# Histograms - distribution of the data
pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/histogram_Pathology_ECX.pdf")
hist(coldata$HIP)
dev.off()

# Remove samples that have NA for pathology (1 sample was removed)
coldata_ECX <- coldata[,c("Genotype", "Age_months", "Histology_no", "ECX")]
coldata_ECX_clean <- na.omit(coldata_ECX)

countdata_pathology <- countdata[,rownames(coldata_ECX_clean)]

# design:
dds <- DESeqDataSetFromMatrix(countData = countdata_pathology, colData = coldata_ECX_clean, design = ~ECX)

# Pre-filtering the data set
# Remove the rows that have no or nearly no information about the amount of gene expression
# Removing rows of the DESeqDataSet that have no counts, or only a single count across all samples
nrow(dds)
dds <- dds[ rowSums(counts(dds)) > 6, ] # filtering for non-expressed and lowly expressed genes (sum of expression accross all samples at least 6 because final n = 6-8 mice per group)
nrow(dds)


##### Differential expression analysis
### WALD TEST ###
dds_Wald <- DESeq(dds, test="Wald")
# The Wald test (also called the Wald Chi-Squared Test) is a way to find out if explanatory variables in a model are significant.
# “Significant” means that they add something to the model; variables that add nothing can be deleted without affecting the model in any meaningful way.
# The test can be used for a multitude of different models including those with binary variables or continuous variables.
# The Wald test is a rough approximation of the Likelihood Ratio Test. However, you can run it with a single model (the LR test requires at least two).
# It is also more broadly applicable than the LRT: often, you can run a Wald in situations where no other test can be run.
# For large values of n, the Wald test is roughly equivalent to the t-test; both tests will reject the same values for large sample sizes.
# The Wald, LRT and Lagrange multiplier tests are all equivalent as sample sizes approach infinity (called “asymptotically equivalent”).
# However, samples of a finite size, especially smaller samples, are likely to give very different results.

dds_Wald # class: DESeqDataSet

resultsNames(dds_Wald)

# Building the results table
# Calling results without any arguments will extract the estimated log2 fold changes and p values for the last variable in the design formula
res_Wald <- results(dds_Wald)
res_Wald
results_Wald_frame <- data.frame(res_Wald, stringsAsFactors=FALSE)
write.csv(results_Wald_frame, file = "Tg4510_res_Wald_PATHOLOGY_ECX.csv") 

stats_Wald <- mcols(dds_Wald)
rownames(stats_Wald) <- rownames(dds_Wald)
head(stats_Wald)
stats_Wald_frame <- data.frame(stats_Wald, stringsAsFactors=FALSE)

# DESeq preserves the original counts in counts(dds) saving the replacement counts as a matrix named replaceCounts in assays(dds).
#replacedcounts_Wald <- assays(dds_Wald)[["replaceCounts"]] # 
#head(replacedcounts_Wald) # class(): matrix

save(dds_Wald, res_Wald, stats_Wald, file="DEseq2_Wald_test_results_Tg4510_PATHOLOGY_ECX.RData")
save(countdata, coldata, file="DEseq2_object_counts_Tg4510_PATHOLOGY.RData") # saves as objects; to open use load()

# Stats Tables (final)
FDR_adj_pathology <- p.adjust(stats_Wald[,"WaldPvalue_ECX"], method = "fdr") # we have calculated the FDR-adj pvalue ourselves because 1) they were not available for all analysis,and 2) from DESeq some were "NA"

stats_table <-cbind(FDR_adj_pathology,
	as.data.frame(stats_Wald[,c("WaldPvalue_ECX", "ECX")]))

sig_PATHOLOGY <- stats_table[which(stats_table[,"FDR_adj_pathology"]<0.05),]
write.csv(sig_PATHOLOGY, file = "Tg4510_sig_PATHOLOGY_ECX.csv")

table_publication_PATHOLOGY <- cbind(FDR_adj_pathology,
	as.data.frame(stats_Wald[,c("WaldPvalue_ECX", "ECX", "WaldStatistic_ECX")]))
dim(table_publication_PATHOLOGY)
rownames(table_publication_PATHOLOGY) <- rownames(res_Wald)
head(table_publication_PATHOLOGY)

genes_Tg4510_PATHOLOGY <- rownames(sig_PATHOLOGY)
length(genes_Tg4510_PATHOLOGY)

table_publication_PATHOLOGY <- table_publication_PATHOLOGY[genes_Tg4510_PATHOLOGY,]
head(table_publication_PATHOLOGY)
write.csv(table_publication_PATHOLOGY, file = "Tg4510_sig_PATHOLOGY_ECX_STATS.csv")