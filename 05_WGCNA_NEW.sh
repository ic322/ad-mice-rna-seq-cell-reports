#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# WGCNA RNA-seq Tg4510 mice
# R version 3.4.3 (2017-11-30)
# WGCNA 1.63

# Followed these instructions: https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/index.html

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")


## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore")) 
#install.packages("WGCNA")

# Load the WGCNA package
library(WGCNA)
library(RColorBrewer)
library(gplots)
library(ggplot2)
library(reshape2)
library("DESeq2")
#library(knitr)
#library(limma)
#library(flashClust)

### 1 Data input, cleaning and pre-processing

## 1.a Loading expression data

# The following setting is important, do not omit.
options(stringsAsFactors = FALSE);

#Read in the data set
raw_counts <- as.data.frame(countdata)

# Take a quick look at what is in the data set:
dim(raw_counts);
names(raw_counts)
names(raw_counts) <- coldata$Tissue_ID
head(raw_counts)

nrow(raw_counts)
raw_counts <- raw_counts[ rowSums(raw_counts) > 6, ] # filtering for non-expressed and lowly expressed genes (sum of expression accross all samples at least 6 because final n = 6-8 mice per group)
nrow(raw_counts)

datExpr0 = as.data.frame(t(raw_counts));
names(datExpr0)

## 1.b Checking data for excessive missing values and identification of outlier RNA-seq samples
# We first check for genes and samples with too many missing values:
gsg = goodSamplesGenes(datExpr0, verbose = 3);
gsg$allOK

# If the last statement returns TRUE, all genes have passed the cuts. If not, we remove the offending genes and samples from the data:
if (!gsg$allOK)
{
# Optionally, print the gene and sample names that were removed:
if (sum(!gsg$goodGenes)>0)
printFlush(paste("Removing genes:", paste(names(datExpr0)[!gsg$goodGenes], collapse = ", ")));
if (sum(!gsg$goodSamples)>0)
printFlush(paste("Removing samples:", paste(rownames(datExpr0)[!gsg$goodSamples], collapse = ", ")));
# Remove the offending genes and samples from the data:
datExpr0 = datExpr0[gsg$goodSamples, gsg$goodGenes]
}

# Next we cluster the samples (in contrast to clustering genes that will come later) to see if there are any obvious outliers.
sampleTree = hclust(dist(datExpr0), method = "average");
# Plot the sample tree: Open a graphic output window of size 12 by 9 inches
# The user should change the dimensions if the window is too large or too small.
# Clustering dendrogram of samples based on their Euclidean distance:
pdf(file = "plots/WGCNA_sampleClustering.pdf", width = 12, height = 15);
par(cex = 0.6);
par(mar = c(0,4,2,0))
plot(sampleTree, main = "Sample clustering to detect outliers", sub="", xlab="", cex.lab = 1.5, cex.axis = 1.5, cex.main = 2)
abline(h = 150000, col = "red"); # Plot a line to show the cut
dev.off()

# If outliers:
# One can remove it by hand, or use an automatic approach. Choose a height cut that will remove the offending sample, say 15 (the red line in the plot), and use a branch cut at that height.
# I plotted the line in line 84

# Determine cluster under the line
clust = cutreeStatic(sampleTree, cutHeight = 150000, minSize = 10)
table(clust)
# clust 1 contains the samples we want to keep.
keepSamples = (clust==1)
datExpr = datExpr0[keepSamples, ]
nGenes = ncol(datExpr)
nSamples = nrow(datExpr)
### We removed 1 sample (ECX29)

# The variable datExpr now contains the expression data ready for network analysis.

## 1.c Loading trait data
# We now read in the trait data and match the samples for which they were measured to the expression samples.
traitData = coldata;
dim(traitData)
names(traitData)

# only keep columns that hold information we need.
allTraits = traitData[, c(2:5)];
allTraits = allTraits[-grep("ECX29", allTraits$Tissue_ID),]; # removed the row correspondent to the sample removed above
dim(allTraits)

# Form a data frame analogous to expression data that will hold the traits.
samples = rownames(datExpr);
traitRows = match(samples, allTraits$Tissue_ID);
datTraits = allTraits[traitRows, -1];
rownames(datTraits) = allTraits[traitRows, 1];

# Sample check
# sample-correlation heatmap
# add a colorbar along the heatmap with sample condition
group_levels <- factor(datTraits$Group_ID, levels = c("Tg4510_WT_2m", "Tg4510_WT_4m", "Tg4510_WT_6m", "Tg4510_WT_8m", "Tg4510_TG_2m", "Tg4510_TG_4m", "Tg4510_TG_6m", "Tg4510_TG_8m"))
num_conditions <- nlevels(group_levels)
palette <- colorRampPalette(brewer.pal(num_conditions, "Set1"))(num_conditions)
cond_colors <- palette[as.integer(group_levels)]

pdf(file = "plots/WGCNA_heatmap_sample_correlations.pdf", width = 10, height = 8)
heatmap.2(cor(raw_counts[,-46]), RowSideColors=cond_colors, trace='none', main='Sample correlations (raw counts)', # removed ECX29
	margins =c(10,14),cexRow =0.6,
	cexCol = 0.6,
	na.rm = TRUE )
legend("topright", legend = unique(levels(group_levels)), col = cond_colors, lty= 1, lwd = 5, cex=.7)
dev.off()

# by genotype
num_conditions <- nlevels(datTraits$Genotype)
palette <- colorRampPalette(brewer.pal(num_conditions, "Set1"))(num_conditions)
cond_colors <- palette[datTraits$Genotype]

pdf(file = "plots/WGCNA_heatmap_sample_correlations_genotype.pdf", width = 10, height = 8)
heatmap.2(cor(raw_counts[,-46]), RowSideColors=cond_colors, trace='none', main='Sample correlations (raw counts)', # removed ECX29
	margins =c(10,14),cexRow =0.6,
	cexCol = 0.6,
	na.rm = TRUE )
legend("topright", legend = unique(datTraits$Genotype), col = cond_colors, lty= 1, lwd = 5, cex=.7)
dev.off()

# We have the expression data in the variable datExpr, and the corresponding clinical traits in the variable datTraits.
# Before we continue with network construction and module detection, we visualize how the clinical traits relate to the sample dendrogram.

# Re-cluster samples
sampleTree2 = hclust(dist(datExpr), method = "average")
# Convert traits to a color representation: white means low, red means high, grey means missing entry
traitColors = labels2colors(datTraits);
# Plot the sample dendrogram and the colors underneath.
pdf(file = "plots/WGCNA_sampleClustering_labels.pdf", width = 12, height = 9)
plotDendroAndColors(sampleTree2,
	traitColors,
	groupLabels = names(datTraits),
	rowText = datTraits,
	rowTextAlignment = "center",
	main = "Sample dendrogram and trait heatmap")
dev.off()
# In the plot, white means a low value, red a high value, and grey a missing entry.


### 2 Automatic construction of the gene network and identification of modules
## 2.a Automatic network construction and module detection
# Allow multi-threading within WGCNA. This helps speed up certain calculations.
# At present this call is necessary for the code to work.
# Any error here may be ignored but you may want to update WGCNA if you see one.
# Caution: skip this line if you run RStudio or other third-party R environments.
# See note above.
enableWGCNAThreads(10) # reduce this not to use all of knight

## Filter low count genes
# See line 53: raw_counts <- raw_counts[ rowSums(raw_counts) > 6, ]
# "We suggest removing features whose counts are consistently low (for example, removing all features that have a count of less than say 10 in more than 90% of the samples) because such low-expressed features tend to reflect noise and correlations based on counts that are mostly zero aren't really meaningful. The actual thresholds should be based on experimental design, sequencing depth and sample counts.""
#numb_samples_fail_per_gene <- colSums(datExpr < 1)
#threshold <- 0.9*nrow(datExpr)
#failed_genes <- which(numb_samples_fail_per_gene > threshold)
#dataExpr_filt <- datExpr[,-failed_genes]

## Log2 transformation
# https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/faq.html:
# "We then recommend a variance-stabilizing transformation. For example, package DESeq2 implements the function varianceStabilizingTransformation which we have found useful, but one could also start with normalized counts (or RPKM/FPKM data) and log-transform them using log2(x+1). For highly expressed features, the differences between full variance stabilization and a simple log transformation are small."
# Most of the methods developed for co-expression network analysis and network inference were written for use with microarray data, including WGCNA!
# Attempting to apply a method such as this to discrete-count RNA-Seq data will not work out well.
# There are a number of methods for working around this, in effect, making RNA-Seq data "look" more like microarray data, but the simplest thing is just to log the data. This will transform our discrete, over-dispersed counts to a more Poisson-like continuous distribution.
# Apply A Variance Stabilizing Transformation (VST) To The Count Data
#log_counts <- log2(datExpr + 1) # this was the transformation suggested by the package
rlog_counts <- t(datExpr)
rlog_counts <- rlog(rlog_counts) # we used rlog() 1) to be consistent with the DESeq2 analysis and 2) because from DESeq2 QC rlog transformation showed more robust results
rlog_counts <- t(rlog_counts)
colnames(rlog_counts) <- colnames(datExpr)
# rlog (DESeq2): "This function transforms the count data to the log2 scale in a way which minimizes differences between samples for rows with small counts, and which normalizes with respect to library size."

# The last step is to save the relevant expression and trait data for use in the next steps.
save(datExpr, rlog_counts, datTraits, file = "exp_data_for_WGCNA.RData")

collectGarbage();

# Let's see how things look after logging the data.
x = melt(as.matrix(t(rlog_counts)))
colnames(x) = c('gene_id', 'sample', 'value')

pdf("plots/WGCNA_rlog_counts.pdf")
ggplot(x, aes(x=value, color=sample)) + geom_density()
dev.off()


# 2.a.1 Choosing the soft-thresholding power: analysis of network topology
# Choose a set of soft-thresholding powers
powers = c(c(1:10), seq(from = 12, to=20, by=2))
# Call the network topology analysis function
sft = pickSoftThreshold(rlog_counts, powerVector = powers, verbose = 5)
# Plot the results:
pdf(file = "plots/WGCNA_power_threshold.pdf", width = 9, height = 5)
par(mfrow = c(1,2))
cex1 = 0.9;
# Scale-free topology fit index as a function of the soft-thresholding power
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2], 
	xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
	main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
	labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.90,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5], 
	xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n", 
	main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
dev.off()

# I chose the power 10, which is the lowest power for which the scale-free topology fit index curve flattens out upon reaching a high value (in this case, roughly 0.90).