#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Module Medians | WGCNA RNA-seq Tg4510 mice from signed network
# https://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/Simulated-07-Membership.pdf

# Following 06_WGCNA_NEW_signed.sh

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")


## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore")) 
#install.packages("WGCNA")

# Load the WGCNA package
library(WGCNA)
library(RColorBrewer)
library(gplots)
library(ggplot2)
library(reshape2)

options(stringsAsFactors = FALSE); # This setting is important, do not omit.

enableWGCNAThreads(10) # reduce this not to use all of knight

### XXX

# load the variables datExpr and datTraits containing the expression and trait data

# Our variables:
#rlog_counts
#datTraits2
#names(signed_net)
#moduleLabels_signed = signed_net$colors # (moduleLabels)
#moduleColors_signed = labels2colors(signed_net$colors) # (moduleColors)
#MEs_signed = signed_net$MEs; # (consMEs)
#geneTree_signed = signed_net$dendrograms[[1]]; # (consTree)
#nGenes_signed = ncol(rlog_counts);
#nSamples_signed = nrow(rlog_counts);

rlog_counts_transposed <- t(rlog_counts)

dynamicColors<-moduleColors_signed
blockwiseMEs<-moduleEigengenes(t(rlog_counts_transposed), dynamicColors)$eigengenes
rownames(blockwiseMEs)<-colnames(rlog_counts_transposed)
write.csv(blockwiseMEs, "ModuleEigengenes_Tg4510_signed.csv")

save(rlog_counts_transposed, dynamicColors, blockwiseMEs, file = "Tg4510_RNAseq_medians.RData")


### save median expression for each sample
Module.medians<-matrix(data = NA, nrow = length(unique(dynamicColors)), ncol = ncol(rlog_counts_transposed))
rownames(Module.medians)<-unique(dynamicColors)
colnames(Module.medians)<-colnames(rlog_counts_transposed)
for(module in unique(dynamicColors)){
	tmp<-rlog_counts_transposed[which(dynamicColors == module),]
	Module.medians[module,]<-apply(tmp, 2, median)
}
write.csv(Module.medians, "ModuleMedian_signed.csv")

pdf("plots/DensityPlot_ModuleMedians.pdf", width = 20, height = 15)
par(mfrow = c(5,5))
#par(mar = c(3,1,1.5,0.5)) # mar – A numeric vector of length 4, which sets the margin sizes in the following order: bottom, left, top, and right. The default is c(5.1, 4.1, 4.1, 2.1).
par(mgp = c(1.5,0.7,0)) # mgp – A numeric vector of length 3, which sets the axis label locations relative to the edge of the inner plot window. The first value represents the location the labels (i.e. xlab and ylab in plot), the second the tick-mark labels, and third the tick marks. The default is c(3, 1, 0).
for(module in unique(dynamicColors)){
	module.x<-NULL
	module.y<-NULL
	tmp<-density(rlog_counts_transposed[which(dynamicColors == module)],)
	module.x<-cbind(module.x, tmp$x)
	module.y<-cbind(module.y, tmp$y)
	plot(module.x, module.y, col = rainbow(11), type = "l", ylim = c(0,max(module.y)), xlab = "Gene expression", ylab = "", main = module)
}
dev.off()