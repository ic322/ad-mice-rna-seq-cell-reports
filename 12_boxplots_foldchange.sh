#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Script to build plot showing difference between TG & WT in each time point Tg4510 mice

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

source("https://bioconductor.org/biocLite.R")

library("DESeq2")

data <- stats_Wald[which(stats_table[,"FDR_adj_LRT"]<0.05),]
a <- abs(data[,"GenotypeTG.Age_months4"])
b <- abs(data[,"GenotypeTG.Age_months6"])
d <- abs(data[,"GenotypeTG.Age_months8"])
y_lim<-range(c(a,b,d))

pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/boxplot_log2FoldChange.pdf")
boxplot(a, xlim=c(0,4), ylim = y_lim, col="#73B5BF", axes = FALSE)
axis(1)
axis(2)
boxplot(b, col="#73B5BF", add=TRUE, at=2)
boxplot(d, col="#73B5BF", add=TRUE, at=3)
dev.off()

library(vioplot)
pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/violinplot_log2FoldChange.pdf")
par(las=1,bty="l")  ## my preferred setting
## set up empty plot
plot(0:1,0:1, type="n", xlim=c(0.5,3.5), ylim=range(c(a,b,d)), axes=FALSE, ann=FALSE)
vioplot(a,b,d, col="#73B5BF", add=TRUE)
axis(side=1, at=1:3, labels=c("4 months", "6 months", "8 months"))
axis(2)
title(xlab = "Comparasion age group/timepoint", ylab = "Log2 fold change (absolute value) relative to 2 months")
dev.off()