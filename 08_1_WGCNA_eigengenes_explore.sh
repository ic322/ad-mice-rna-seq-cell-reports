#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Eigengens - associations | WGCNA RNA-seq J20 mice from signed network

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")


## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore")) 
#install.packages("WGCNA")

# Load the WGCNA package
library(WGCNA)
library(RColorBrewer)
library(gplots)
library(ggplot2)
library(reshape2)

options(stringsAsFactors = FALSE); # This setting is important, do not omit.

enableWGCNAThreads(10) # reduce this not to use all of knight

# Our variables:
#rlog_counts
#datTraits2
#names(signed_net)
#moduleLabels_signed = signed_net$colors # (moduleLabels)
#moduleColors_signed = labels2colors(signed_net$colors) # (moduleColors)
#MEs_signed = signed_net$MEs; # (consMEs)
#geneTree_signed = signed_net$dendrograms[[1]]; # (consTree)
#nGenes_signed = ncol(rlog_counts);
#nSamples_signed = nrow(rlog_counts);
#rlog_counts_transposed <- t(rlog_counts)
#dynamicColors<-moduleColors_signed
#blockwiseMEs<-moduleEigengenes(t(rlog_counts_transposed), dynamicColors)$eigengenes

head(datTraits2)
identical(rownames(rlog_counts), rownames(datTraits2))
signed_net$MEs # module eigengenes
signed_net$colors # module assignments

# how many modules  were identified and module size
table(signed_net$colors)
table(labels2colors(signed_net$colors))
write.csv(table(labels2colors(signed_net$colors)), file = "WGCNA_genespermodule_colours.csv")

### Relating modules to external clinical traits
# see http://labs.genetics.ucla.edu/horvath/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/FemaleLiver-03-relateModsToExt.pdf for more detail
# "gebwnet_netree"
geneTree_signed # gebwnet_netree = bwnet_extend$dendrograms[[1]]
# "MEs"
MEs_signed # MEs = bwnet_extend$MEs
# "moduleColors"
moduleColors_signed # moduleColors = labels2colors(bwnet_extend$colors)
# "moduleLabels"
moduleLabels_signed # moduleLabels = bwnet_extend$colors

# Define numbers of genes and samples
nGenes = nrow(rlog_counts_transposed)
nSamples =  ncol(rlog_counts_transposed)

# Recalculate MEs with color labels
dynamicColors # dynamicColors<-labels2colors(moduleLabels)
blockwiseMEs # blockwiseMEs = moduleEigengenes(t(betas), dynamicColors)$eigengenes
blockwiseMEs_2 <- blockwiseMEs[,-7] # remove the grey module

#how many modules
dim(blockwiseMEs_2)
# [1] 62 20

identical(rownames(datTraits2), rownames(rlog_counts))
identical(rownames(datTraits2), colnames(rlog_counts_transposed))


### BOXPLOTS of Results
# Genotype
pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/MEPlotsGenotype.pdf", width=10, height=10)
par(mfrow = c(2,2))
boxplot(blockwiseMEs$MEblack~datTraits2$Genotype,
	main="Black module",
	ylab="Module Eigengene",
	col=(c("white","grey13")))
	legendtitle=c("P = 0.0347")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEsalmon~datTraits2$Genotype,
	main="Salmon module",
	ylab="Module Eigengene",
	col=(c("white","salmon")))
	legendtitle=c("P = 3.58e-06")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEturquoise~datTraits2$Genotype,
	main="Turquoise module",
	ylab="Module Eigengene",
	col=(c("white","turquoise")))
	legendtitle=c("P = 3.04e-10")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEgrey60~datTraits2$Genotype,
	main="Grey60 module",
	ylab="Module Eigengene",
	col=(c("white","grey60")))
	legendtitle=c("P = 0.00663")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEgreen~datTraits2$Genotype,
	main="Green module",
	ylab="Module Eigengene",
	col=(c("white","green")))
	legendtitle=c("P = 0.0358")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEpurple~datTraits2$Genotype,
	main="Purple module",
	ylab="Module Eigengene",
	col=(c("white","purple")))
	legendtitle=c("P = 0.000121")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEyellow~datTraits2$Genotype,
	main="Yellow module",
	ylab="Module Eigengene",
	col=(c("white","yellow")))
	legendtitle=c("P = 0.00152")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MElightcyan~datTraits2$Genotype,
	main="Light cyan module",
	ylab="Module Eigengene",
	col=(c("white","light cyan")))
	legendtitle=c("P = 1.29e-05")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEred~datTraits2$Genotype,
	main="Red module",
	ylab="Module Eigengene",
	col=(c("white","red")))
	legendtitle=c("P = 1.43e-10")
	legend("topright", legend=legendtitle,bty = "n")
dev.off()

# Group ID
pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/MEPlotsGroupID.pdf", width=10, height=10)
par(mfrow = c(2,2))
boxplot(blockwiseMEs$MEblack~datTraits2$Group_ID,
	main="Black module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "black", "black", "black", "black")))
	legendtitle=c("P = 0.036")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEturquoise~datTraits2$Group_ID,
	main="Turquoise module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "turquoise", "turquoise", "turquoise", "turquoise")))
	legendtitle=c("P = 4.23e-06")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEgreen~datTraits2$Group_ID,
	main="Green module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "green", "green", "green", "green")))
	legendtitle=c("P = 0.0558")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEpurple~datTraits2$Group_ID,
	main="Purple module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "purple", "purple", "purple", "purple")))
	legendtitle=c("P = 0.0451")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEyellow~datTraits2$Group_ID,
	main="Yellow module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "yellow", "yellow", "yellow", "yellow")))
	legendtitle=c("P = 0.00177")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MElightcyan~datTraits2$Group_ID,
	main="Light cyan module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "light cyan", "light cyan", "light cyan", "light cyan")))
	legendtitle=c("P = 0.0247")
	legend("topright", legend=legendtitle,bty = "n")

boxplot(blockwiseMEs$MEred~datTraits2$Group_ID,
	main="Red module",
	ylab="Module Eigengene",
	names=c("WT 2m","WT 4m","WT 6m", "WT 8m", "TG 2m", "TG 4m", "TG 6m", "TG 8m"),
	las = 2,
	col=(c("white", "white", "white", "white", "red", "red", "red", "red")))
	legendtitle=c("P = 0.00222")
	legend("topright", legend=legendtitle,bty = "n")
dev.off()


### Look in module
# gene significance and identifying hub genes

geneModuleMembership = as.data.frame(cor(t(rlog_counts_transposed), blockwiseMEs, use = "p")); #correlation of each probe with eigengenes
MMPvalue = as.data.frame(corPvalueStudent(as.matrix(geneModuleMembership), nSamples)); #dataframe of P value correlations

modNames = substring(names(blockwiseMEs), 3)
names(geneModuleMembership) = paste("MM", modNames, sep="");
names(MMPvalue) = paste("p.MM", modNames, sep="");

#geneTraitSignificance = as.data.frame(cor(t(rlog_counts_transposed), PATHOLOGY, use = "p")); #####################################################
#GSPvalue = as.data.frame(corPvalueStudent(as.matrix(geneTraitSignificance), nSamples));
# use the results of the linear model for geneTraitSignificance and GSPvalue

# Get genes in modules
geneInfoALL<-data.frame(probeIDs = rownames(rlog_counts_transposed), moduleColor=dynamicColors, ModuleMembershipCor=geneModuleMembership, ModuleMembershipPvalue=MMPvalue)

# MEred
#head(rownames(geneModuleMembership$MMred))
head(MMPvalue$p.MMred)
red <- which(geneInfoALL$moduleColor=="red")
geneInfoALL_red <- geneInfoALL[red,]
nrow(geneInfoALL_red)
rownames(geneInfoALL_red)
write.csv(geneInfoALL_red, file = "WGCNA_MM_red.csv")

matched <- match(geneInfoALL_red$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_red <- rlog_counts_transposed[matched,]
dim(rlog_counts_red)

# MEpurple
#head(rownames(geneModuleMembership$MMpurple))
head(MMPvalue$p.MMpurple)
purple <- which(geneInfoALL$moduleColor=="purple")
geneInfoALL_purple <- geneInfoALL[purple,]
nrow(geneInfoALL_purple)
rownames(geneInfoALL_purple)
write.csv(geneInfoALL_purple, file = "WGCNA_MM_purple.csv")

matched <- match(geneInfoALL_purple$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_purple <- rlog_counts_transposed[matched,]
dim(rlog_counts_purple)

# MEgreen
#head(rownames(geneModuleMembership$MMgreen))
head(MMPvalue$p.MMgreen)
green <- which(geneInfoALL$moduleColor=="green")
geneInfoALL_green <- geneInfoALL[green,]
nrow(geneInfoALL_green)
rownames(geneInfoALL_green)
write.csv(geneInfoALL_green, file = "WGCNA_MM_green.csv")

matched <- match(geneInfoALL_green$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_green <- rlog_counts_transposed[matched,]
dim(rlog_counts_green)

# MElightcyan
#head(rownames(geneModuleMembership$MMlightcyan))
head(MMPvalue$p.MMlightcyan)
lightcyan <- which(geneInfoALL$moduleColor=="lightcyan")
geneInfoALL_lightcyan <- geneInfoALL[lightcyan,]
nrow(geneInfoALL_lightcyan)
rownames(geneInfoALL_lightcyan)
write.csv(geneInfoALL_lightcyan, file = "WGCNA_MM_lightcyan.csv")

matched <- match(geneInfoALL_lightcyan$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_lightcyan <- rlog_counts_transposed[matched,]
dim(rlog_counts_lightcyan)

# MEyellow
#head(rownames(geneModuleMembership$MMyellow))
head(MMPvalue$p.MMyellow)
yellow <- which(geneInfoALL$moduleColor=="yellow")
geneInfoALL_yellow <- geneInfoALL[yellow,]
nrow(geneInfoALL_yellow)
rownames(geneInfoALL_yellow)
write.csv(geneInfoALL_yellow, file = "WGCNA_MM_yellow.csv")

matched <- match(geneInfoALL_yellow$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_yellow <- rlog_counts_transposed[matched,]
dim(rlog_counts_yellow)

# MEturquoise
#head(rownames(geneModuleMembership$MMturquoise))
head(MMPvalue$p.MMturquoise)
turquoise <- which(geneInfoALL$moduleColor=="turquoise")
geneInfoALL_turquoise <- geneInfoALL[turquoise,]
nrow(geneInfoALL_turquoise)
rownames(geneInfoALL_turquoise)
write.csv(geneInfoALL_turquoise, file = "WGCNA_MM_turquoise.csv")

matched <- match(geneInfoALL_turquoise$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_turquoise <- rlog_counts_transposed[matched,]
dim(rlog_counts_turquoise)

# MEblack
#head(rownames(geneModuleMembership$MMblack))
head(MMPvalue$p.MMblack)
black <- which(geneInfoALL$moduleColor=="black")
geneInfoALL_black <- geneInfoALL[black,]
nrow(geneInfoALL_black)
rownames(geneInfoALL_black)
write.csv(geneInfoALL_black, file = "WGCNA_MM_black.csv")

matched <- match(geneInfoALL_black$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_black <- rlog_counts_transposed[matched,]
dim(rlog_counts_black)

# MEgrey60
#head(rownames(geneModuleMembership$MMgrey60))
head(MMPvalue$p.MMgrey60)
grey60 <- which(geneInfoALL$moduleColor=="grey60")
geneInfoALL_grey60 <- geneInfoALL[grey60,]
nrow(geneInfoALL_grey60)
rownames(geneInfoALL_grey60)
write.csv(geneInfoALL_grey60, file = "WGCNA_MM_grey60.csv")

matched <- match(geneInfoALL_grey60$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_grey60 <- rlog_counts_transposed[matched,]
dim(rlog_counts_grey60)

# MEsalmon
#head(rownames(geneModuleMembership$MMsalmon))
head(MMPvalue$p.MMsalmon)
salmon <- which(geneInfoALL$moduleColor=="salmon")
geneInfoALL_salmon <- geneInfoALL[salmon,]
nrow(geneInfoALL_salmon)
rownames(geneInfoALL_salmon)
write.csv(geneInfoALL_salmon, file = "WGCNA_MM_salmon.csv")

matched <- match(geneInfoALL_salmon$probeIDs,rownames(rlog_counts_transposed))
rlog_counts_salmon <- rlog_counts_transposed[matched,]
dim(rlog_counts_salmon)


### Scatter plots eigengenes ~ pathology
## BLACK module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_black.pdf")
module = "black"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEblack"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_black.pdf")
module = "black"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEblack"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()


## SALMON module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_salmon.pdf")
module = "salmon"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEsalmon"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_salmon.pdf")
module = "salmon"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEsalmon"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## TURQUOISE module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_turquoise.pdf")
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEturquoise"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# CA1
pdf(file = "plots/WGCNA_ME_pathology_CA1_turquoise.pdf")
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEturquoise"],
	datTraits3$Pathology_CA1,
	corOptions = "use = 'p'",
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in CA1",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Thalamus
pdf(file = "plots/WGCNA_ME_pathology_THAL_turquoise.pdf")
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEturquoise"],
	datTraits3$Pathology_THAL,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in thalamus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_turquoise.pdf")
module = "turquoise"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEturquoise"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## BLUE module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_blue.pdf")
module = "blue"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEblue"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_blue.pdf")
module = "blue"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEblue"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## YELLOW module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_yellow.pdf")
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEyellow"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# CA3
pdf(file = "plots/WGCNA_ME_pathology_CA3_yellow.pdf")
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEyellow"],
	datTraits3$Pathology_CA3,
	corOptions = "use = 'p'",
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in CA3",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Thalamus
pdf(file = "plots/WGCNA_ME_pathology_THAL_yellow.pdf")
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEyellow"],
	datTraits3$Pathology_THAL,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in thalamus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_yellow.pdf")
module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEyellow"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## LIGHT CYAN module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_lightcyan.pdf")
module = "lightcyan"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MElightcyan"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# CA1
pdf(file = "plots/WGCNA_ME_pathology_CA1_lightcyan.pdf")
module = "lightcyan"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MElightcyan"],
	datTraits3$Pathology_CA1,
	corOptions = "use = 'p'",
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in CA1",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Thalamus
pdf(file = "plots/WGCNA_ME_pathology_THAL_lightcyan.pdf")
module = "lightcyan"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MElightcyan"],
	datTraits3$Pathology_THAL,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in thalamus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_lightcyan.pdf")
module = "lightcyan"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MElightcyan"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## RED module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_red.pdf")
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEred"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Visual cortex
pdf(file = "plots/WGCNA_ME_pathology_visualCTX_red.pdf")
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEred"],
	datTraits3$Pathology_visual_CTX,
	corOptions = "use = 'p'",
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in visual cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Thalamus
pdf(file = "plots/WGCNA_ME_pathology_THAL_red.pdf")
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEred"],
	datTraits3$Pathology_THAL,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in thalamus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_red.pdf")
module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEred"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

## PURPLE module
# Hippocampus
pdf(file = "plots/WGCNA_ME_pathology_HIP_purple.pdf")
module = "purple"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEpurple"],
	datTraits3$Pathology_HIP,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in hippocampus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# CTX
pdf(file = "plots/WGCNA_ME_pathology_CTX_purple.pdf")
module = "purple"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEpurple"],
	datTraits3$Pathology_CTX,
	corOptions = "use = 'p'",
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Thalamus
pdf(file = "plots/WGCNA_ME_pathology_THAL_purple.pdf")
module = "purple"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEpurple"],
	datTraits3$Pathology_THAL,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in thalamus",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

# Entorhinal Cortex
pdf(file = "plots/WGCNA_ME_pathology_ECX_purple.pdf")
module = "purple"
column = match(module, modNames);
moduleGenes = moduleColors==module;
sizeGrWindow(7, 7);
par(mfrow = c(1,1));
verboseScatterplot(MEs_signed[,"MEpurple"],
	datTraits3$ECX,
	xlab = paste("Module eigengene in", module, "module"),
	ylab = "Tau pathology in entorhinal cortex",
	xlim=c(-0.4,0.4),
	ylim=c(0,40),
	main = paste("Module eigengene vs. pathology\n"),
	cex.main = 1.2, cex.lab = 1.2, cex.axis = 1.2, col = "black", bg=c("white", module)[datTraits2$Genotype], pch = 21)
dev.off()

### Explore modules correlated with pathology
data <- as.data.frame(t(blockwiseMEs))
colnames(data) <- rownames(blockwiseMEs)

datTraits2$Age_months <- as.factor(datTraits2$Age_months)
datTraits2$Genotype <- as.factor(datTraits2$Genotype)
datTraits2$Genotype <- relevel(datTraits2$Genotype, "WT")
levels(pat_data$Genotype)

# MEblack
module <- "MEblack"
modulecolour <- "black"
data_plot <- cbind(datTraits2[,c("Age_months","Genotype")], t(data[module,]))
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,module], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,module], na.rm=TRUE))
}
pdf("plots/WGCNA_MEblack_group.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3)) # bottom, left, top, and right
plot(y=data[module,], x=data_plot$Age_months,
	col="black",
	bg=c("white", modulecolour)[datTraits2$Genotype],
	pch = 21,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(-0.4,0.4),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main="Black module", xlab = "Age (months)", ylab="Module Eigengene", mgp=c(4,1,0), cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

# MEsalmon
module <- "MEsalmon"
modulecolour <- "salmon"
data_plot <- cbind(datTraits2[,c("Age_months","Genotype")], t(data[module,]))
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,module], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,module], na.rm=TRUE))
}
pdf("plots/WGCNA_MEsalmon_group.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3)) # bottom, left, top, and right
plot(y=data[module,], x=data_plot$Age_months,
	col="black",
	bg=c("white", modulecolour)[datTraits2$Genotype],
	pch = 21,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(-0.4,0.4),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main="Salmon module", xlab = "Age (months)", ylab="Module Eigengene", mgp=c(4,1,0), cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

# MEblue
module <- "MEblue"
modulecolour <- "blue"
data_plot <- cbind(datTraits2[,c("Age_months","Genotype")], t(data[module,]))
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,module], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,module], na.rm=TRUE))
}
pdf("plots/WGCNA_MEblue_group.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3)) # bottom, left, top, and right
plot(y=data[module,], x=data_plot$Age_months,
	col="black",
	bg=c("white", modulecolour)[datTraits2$Genotype],
	pch = 21,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(-0.4,0.4),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main="Blue module", xlab = "Age (months)", ylab="Module Eigengene", mgp=c(4,1,0), cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

# MElightcyan
module <- "MElightcyan"
modulecolour <- "lightcyan"
data_plot <- cbind(datTraits2[,c("Age_months","Genotype")], t(data[module,]))
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,module], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,module], na.rm=TRUE))
}
pdf("plots/WGCNA_MElightcyan_group.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3)) # bottom, left, top, and right
plot(y=data[module,], x=data_plot$Age_months,
	col="black",
	bg=c("white", modulecolour)[datTraits2$Genotype],
	pch = 21,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(-0.4,0.4),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main="Lightcyan module", xlab = "Age (months)", ylab="Module Eigengene", mgp=c(4,1,0), cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()

# MEpurple
module <- "MEpurple"
modulecolour <- "purple"
data_plot <- cbind(datTraits2[,c("Age_months","Genotype")], t(data[module,]))
means_WT <- c()
means_TG <- c()
for (age in c("2","4","6","8")) {
	means_WT <- c(means_WT, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="WT"))[,module], na.rm=TRUE))
	means_TG <- c(means_TG, mean(subset(data_plot, subset=(data_plot$Age_months==age & data_plot$Genotype=="TG"))[,module], na.rm=TRUE))
}
pdf("plots/WGCNA_MEpurple_group.pdf", width=10, height=10)
#dev.new(width=10, height=10)
par(mar=c(6,6,5,3)) # bottom, left, top, and right
plot(y=data[module,], x=data_plot$Age_months,
	col="black",
	bg=c("white", modulecolour)[datTraits2$Genotype],
	pch = 21,
	cex=2,
	xlim=c(0.5,4.5),
	ylim=c(-0.4,0.4),
	axes=FALSE, ann=FALSE)
axis(side=1, at=1:4, labels=c("2", "4", "6", "8"), cex.axis=2)
axis(2, cex.axis=2)
title(main="Purple module", xlab = "Age (months)", ylab="Module Eigengene", mgp=c(4,1,0), cex.main=2, cex.lab=2)
lines(means_WT, lty=2, col="black", lwd=3)
lines(means_TG, lty=2, col=color_Tg4510_TG, lwd=3)
dev.off()