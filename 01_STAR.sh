#!/bin/bash

# Isabel Castanho (I.S.Castanho@exeter.ac.uk)

# Script to align RNA-seq samples from Tg4510 mice using STAR

### Trimming using fastq-mcf - samples were trimmed by the Sequencing Service (Paul ONeill - P.A.ONeill@exeter.ac.uk)
## From script filter_paired_reads_mcf_ribofilter.sh:
# Need to remove ribosomal sequences first
#bowtie2 -p 8 --local -X 1000 --un-conc noribo -x /bioinformatics/reference/ribosomal/representative_ribo_set_plus_LSU.fasta -1 $1 -2 $2 > /dev/null
#fastq-mcf /bioinformatics/adaptors/adaptors.fasta noribo.1 -o $1.filtered noribo.2 -o $2.filtered -q 20 -l 35 -k 0  -p 15  --max-ns 0 -m 3
#rm -f noribo.*
# /usr/local/scripts/production_scripts/filtering/sync_pe_reads.pl $1.filtered  $2.filtered
#fastqc --extract -t 8 $1.filtered $2.filtered
#md5sum $1.filtered > $1.filtered.md5sum
#md5sum $2.filtered> $2.filtered.md5sum


#cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/

### Mapping using STAR # Verison: STAR_2.5.3a
# https://github.com/alexdobin/STAR
# Mouse (Mus musculus): GRCm38.84 (Ensemble version 84, which was done on GRCm38.p4; mm10 genome)

# Get latest STAR source from releases
#wget https://github.com/alexdobin/STAR/archive/2.5.3a.tar.gz
#tar -xzf 2.5.3a.tar.gz

# Generate the genome files.
#cd /mnt/data1/isabel/reference/
#/mnt/data1/isabel/STAR-2.5.3a/bin/Linux_x86_64/STAR --runMode genomeGenerate --genomeDir /mnt/data1/isabel/reference/ --genomeFastaFiles  /mnt/data1/isabel/reference/genome.fa  --sjdbGTFfile /mnt/data1/isabel/reference/genes.gtf --sjdbOverhang 100 --runThreadN 8
# This should take 1-2 hours and will write STAR genome files into genomepath directory.

### removed samples K22, L20, and O17 (wrong genotype!) ###

# Mapping the reads
PATHS='/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_filtered/*/'

for path in $PATHS
do
	IFS='/' read -ra directoryNames <<< "$path"
	sampleName=${directoryNames[7]}

	echo "Processing sample $sampleName"
	
	dirpath=${path%/*} # ${path%/*} is the full path to the directory that has the files 

	F=$(find $dirpath | grep "fastq.filtered" | grep "R1") 
	R=$(find $dirpath | grep "fastq.filtered" | grep "R2")
	echo $F
	echo $R
	echo -e '\n'

	#STAR
	/mnt/data1/isabel/STAR-2.5.3a/bin/Linux_x86_64/STAR --runThreadN 64 --genomeDir /mnt/data1/isabel/reference/ --readFilesIn $F $R --outFileNamePrefix $sampleName
	
	# samtools
	samtools view -bS $sampleName"Aligned.out.sam" > $sampleName".bam"
	echo -e '\n'
done