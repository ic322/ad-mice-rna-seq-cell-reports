#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Script to plot Tg4510 significant genes against J20 genes (overlap same genes from the 2 mouse models)
# Asigned test (binom test)
# the binomial test is an exact test of the statistical significance of deviations from a theoretically expected distribution of observations into two categories
# One common use of the binomial test is in the case where the null hypothesis is that two categories are equally likely to occur

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# J20 data: /mnt/data1/isabel/RNA-seq/analysis_isabel_new/J20_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

source("https://bioconductor.org/biocLite.R")

library("DESeq2")


### Genes from genotype effect -> log2FoldChange
## Tg4510 genes
#load(file="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/J20_counts/DEseq2_Wald_test_results_J20.RData")
#J20_genotype <- res_Wald ################ WRONG -> see finalplot_RNA-seq_Tg4510.R!!! (Updated!) ################

#load(file="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/DEseq2_Wald_test_results_Tg4510.RData")
#Tg4510_genotype <- res_Wald ################ WRONG -> see finalplot_RNA-seq_Tg4510.R!!! (Updated!) ################

dim(Tg4510_genotype)
dim(J20_genotype)

genes_Tg4510_genotype <- rownames(sig_genotype)
length(genes_Tg4510_genotype)

Tg4510_genotype <- Tg4510_genotype[genes_Tg4510_genotype,]
dim(Tg4510_genotype)

# Make sure we have the same genes for both models
genes <- intersect(rownames(Tg4510_genotype), rownames(J20_genotype))
Tg4510_genotype <- Tg4510_genotype[genes,]
J20_genotype <- J20_genotype[genes,]

dim(Tg4510_genotype)
head(Tg4510_genotype)
dim(J20_genotype)
head(J20_genotype)

identical(rownames(Tg4510_genotype),rownames(J20_genotype)) # should be TRUE now

# plot signif genes from Tg4510 genotype effect againts same genes in J20
threshold <- 0.05

#ntotal <- sum(Tg4510_genotype$padj < threshold, na.rm=TRUE) # number of sig genes I previously identified? should be equal or less...
ntotal <- nrow(Tg4510_genotype) # number of sig genes I previously identified? should be equal or less...
#nconsistent <- sum(sign(Tg4510_genotype$log2FoldChange[which(Tg4510_genotype$padj < threshold)]) == sign(J20_genotype$log2FoldChange[which(Tg4510_genotype$padj < threshold)]), na.rm=TRUE) # list of T and F - sum will give me the number of times T
nconsistent <- sum(sign(Tg4510_genotype$log2FoldChange) == sign(J20_genotype$log2FoldChange)) # list of T and F - sum will give me the number of times T

test <- binom.test(nconsistent, ntotal, 0.5)

pvalue <- test[3]
pvalue <- signif(as.numeric(pvalue), 3)
pvalue # p-value = 0.468
probability <- test[5]
probability <- signif(as.numeric(probability), 3)
probability # p = 0.468

pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/scatterplot_binomtest_Tg4510_J20_genotype_log2FoldChange.pdf")
plot(Tg4510_genotype$log2FoldChange[which(Tg4510_genotype$padj < threshold)], J20_genotype$log2FoldChange[which(Tg4510_genotype$padj < threshold)],
	main = "Overlap of genes for genotype effect (log2FoldChange)",
	#sub = paste("p-value = ", pvalue, "; probability = ", probability),
	xlab = "Tg4510 DEGs for genotype effect",
	ylab = "J20 genes for genotype effect",
	pch = 20,
	cex = 0.5)
abline(v=0)
abline(h=0)
legendtitle <- paste("p-value = ", pvalue, "; probability = ", probability)
legend("bottomright", legend=legendtitle, bty = "n")
dev.off()


### Genes from progression effect -> log2FoldChange
load(file="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/J20_counts/DEseq2_LRT_test_results_J20.RData")
J20_interaction <- res_LRT

load(file="/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/DEseq2_LRT_test_results_Tg4510.RData")
Tg4510_interaction <- res_LRT

dim(Tg4510_interaction)
dim(J20_interaction)

genes_Tg4510_interaction <- rownames(sig_LRT)
length(genes_Tg4510_interaction)

Tg4510_interaction<- Tg4510_interaction[genes_Tg4510_interaction,]
dim(Tg4510_interaction)

# Make sure we have the same genes for both models
genes <- intersect(rownames(Tg4510_interaction), rownames(J20_interaction))
Tg4510_interaction <- Tg4510_interaction[genes,]
J20_interaction <- J20_interaction[genes,]

dim(Tg4510_interaction)
head(Tg4510_interaction)
dim(J20_interaction)
head(J20_interaction)

identical(rownames(Tg4510_interaction),rownames(J20_interaction)) # should be TRUE now

# plot signif genes from Tg4510 interaction effect againts same genes in J20
threshold <- 0.05

#ntotal <- sum(Tg4510_interaction$padj < threshold, na.rm=TRUE) # number of sig genes I previously identified? should be equal or less...
ntotal <- nrow(Tg4510_interaction) # number of sig genes I previously identified? should be equal or less...
#nconsistent <- sum(sign(Tg4510_interaction$log2FoldChange[which(Tg4510_interaction$padj < threshold)]) == sign(J20_interaction$log2FoldChange[which(Tg4510_interaction$padj < threshold)]), na.rm=TRUE) # list of T and F - sum will give me the number of times T
nconsistent <- sum(sign(Tg4510_interaction$log2FoldChange) == sign(J20_interaction$log2FoldChange)) # list of T and F - sum will give me the number of times T

test <- binom.test(nconsistent, ntotal, 0.5)

pvalue <- test[3]
pvalue <- signif(as.numeric(pvalue), 3)
probability <- test[5]
probability <- signif(as.numeric(probability), 3)

# log2FoldChange
pdf("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/plots/scatterplot_binomtest_Tg4510_J20_interaction_log2FoldChange.pdf")
plot(Tg4510_interaction$log2FoldChange[which(Tg4510_interaction$padj < threshold)], J20_interaction$log2FoldChange[which(Tg4510_interaction$padj < threshold)],
	main = "Overlap of genes for interaction effect (log2FoldChange)",
	#sub = paste("p-value = ", pvalue, "; probability = ", probability),
	xlab = "Tg4510 DEGs for interaction effect",
	ylab = "J20 genes for interaction effect",
	pch = 20,
	cex = 0.5)
abline(v=0)
abline(h=0)
legendtitle <- paste("p-value = ", pvalue, "; probability = ", probability)
legend("bottomright", legend=legendtitle,bty = "n")
dev.off()


# List of genes that are significant in J20 [bonferroni corrected for number of significant genes in Tg4510 (p/x)]
# We want to work out of the 1762 Tg4510 genes we've looked up in J20, which show p < (0.05/1762)
genes_J20 <- J20_interaction[which(J20_interaction$pvalue < (0.05/nrow(Tg4510_interaction))),]
write.csv(genes_J20, file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/binomtest_Tg4510_J20_interaction.csv")