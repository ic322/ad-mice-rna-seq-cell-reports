#Isabel Castanho I.S.Castanho@exeter.ac.uk

# WGCNA RNA-seq Tg4510 mice # SIGNED NETWORK
# Addition of ECX Pathology to 08_WGCNA_eigengenes_explore.sh

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")


## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite(c("AnnotationDbi", "impute", "GO.db", "preprocessCore")) 
#install.packages("WGCNA")

# Load the WGCNA package
library(WGCNA)
library(RColorBrewer)
library(gplots)
library(ggplot2)
library(reshape2)

options(stringsAsFactors = FALSE); # This setting is important, do not omit.

enableWGCNAThreads(10) # reduce this not to use all of knight


### 3 Relating modules to traits
## 3.a Quantifying module–trait associations
# In this analysis we would like to identify modules that are significantly associated with the measured clinical traits.
# Since we already have a summary profile (eigengene) for each module, we simply correlate eigengenes with external traits and look for the most significant associations

# Define numbers of genes and samples
nGenes_signed = ncol(rlog_counts);
nSamples_signed = nrow(rlog_counts);
# Recalculate MEs with color labels
MEs0_signed = moduleEigengenes(rlog_counts, moduleColors_signed)$eigengenes
MEs_signed = orderMEs(MEs0_signed)

## Correlations with pathology (burden) [We didn't have pathology data in coldata before, so we used an updated .csv file and created a new variable]
datTraits3 <- read.csv("Tg4510_coldata_WGCNA_ECX.csv", row.names=1, stringsAsFactors=FALSE)
colnames(datTraits3)
rownames(datTraits3)
dim(datTraits3)
head(datTraits3)

identical((datTraits3$Tissue_ID), rownames(rlog_counts))
identical((datTraits3$Tissue_ID), rownames(datExpr))

# remove ECX29 [see 05_WGCNA_NEW.sh]
datTraits3 = datTraits3[-grep("ECX29", datTraits3$Tissue_ID),]; # removed the row correspondent to the sample removed above

identical((datTraits3$Tissue_ID), rownames(rlog_counts))
identical((datTraits3$Tissue_ID), rownames(datExpr))

rownames(datTraits3) = rownames(rlog_counts)
datTraits3 = datTraits3[7:17];
head(datTraits3)

moduleTraitCor = cor(MEs_signed, datTraits3, use = "p");
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples);

# a suitable graphical representation will help in reading the table. We color code each association by the correlation value:
# Will display correlations and their p-values
textMatrix = paste(signif(moduleTraitCor, 2), "\n(",
signif(moduleTraitPvalue, 1), ")", sep = "");
dim(textMatrix) = dim(moduleTraitCor)

# Display the correlation values within a heatmap plot
pdf(file = "plots/WGCNA_correlations_tau_pathology_ECX.pdf", width = 10, height = 10)
#dev.new(width=10, height=10)
par(mar = c(8, 8, 3, 1)); # bottom, left, top, and right
labeledHeatmap(Matrix = moduleTraitCor,
	xLabels = c("Entorhinal Cortex", "Hippocampus", "CA1", "CA3", "DG", "Whole cortex", "M2", "M1", "Visual cortex", "Retrosplenial cortex", "Thalamus"),
	yLabels = names(MEs_signed),
	ySymbols = names(MEs_signed),
	colorLabels = FALSE,
	colors = blueWhiteRed(50),
	textMatrix = textMatrix,
	setStdMargins = FALSE,
	cex.text = 1,
	zlim = c(-1,1),
	main = "Module-trait relationships with tau pathology")
dev.off()
# Module-trait associations. Each row corresponds to a module eigengene, column to a trait. Each cell contains the corresponding correlation and p-value.
# The table is color-coded by correlation according to the color legend.