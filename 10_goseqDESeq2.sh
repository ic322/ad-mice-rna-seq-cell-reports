#!/bin/bash

#Isabel Castanho I.S.Castanho@exeter.ac.uk

# Functional annotation and gene ontology | goseq DEGs Tg4510 mice

# Following 03_DESeq2_NEW.sh

cd /mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/

# open R
R

# setwd and open packages
setwd("/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/")

## SETUP
source("http://bioconductor.org/biocLite.R") 
biocLite("BiocUpgrade") # goseq was not working properly because of the version of bioconductor ### Still didn't solve it, but just for function supportedOrganisms()

library(DESeq2) # DESeq2_1.16.1
library(GO.db)
library(goseq) # goseq_1.30.0 
library (org.Mm.eg.db)
library(annotate)
library(geneLenDataBase)

### DESeq2 data
# Our variables
#res_Wald
#sig_genotype # FDR-adjusted DEGs
#res_LRT 
#sig_LRT # FDR-adjusted DEGs

## Genotype effect
# Constructing a gene vector for "goseq"
assayed_genes <- rownames(res_Wald)
de_genes <- rownames(sig_genotype)
genes_vector=as.integer(assayed_genes%in%de_genes)
names(genes_vector)=assayed_genes
table(genes_vector)

# To see which genome/gene identifier combinations are in the local database
supportedOrganisms() ########################## ERROR ##########################

supportedOrganisms()[supportedOrganisms()$Genome=="mm10",]

# Fitting the Probability Weighting Function (PWF)
pwf=nullp(genes_vector,"mm10","geneSymbol")
head(pwf)

# nullp plots the resulting fit, allowing verification of the goodness of fit before continuing the analysis
# The output of nullp contains all the data used to create the PWF, as well as the PWF itself.
# It is a data frame with 3 columns, named ”DEgenes”, ”bias.data” and ”pwf” with the rownames set to the gene names.
# Each row corresponds to a gene with the DEgenes column specifying if the gene is DE (1 for DE, 0 for not DE), the bias.data column giving the numeric value of the DE bias
# being accounted for (usually the gene length or number of counts) and the pwf column giving the genes value on the probability weighting function.

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/goseq/pwf_Tg4510_genotype.pdf")
plotPWF(pwf)
dev.off()

# Using the Wallenius approximation
GO_wall=goseq(pwf,"mm10","geneSymbol")
# For 2543 genes, we could not find any categories. These genes will be excluded.
head(GO_wall)
# The resulting object is ordered by GO category over representation amongst DE genes.

# To identify categories significantly enriched/unenriched below some p-value cutoff, it is necessary to first apply some kind of multiple hypothesis testing correction.
# For example, GO categories over enriched using a .05 FDR cutoff [Benjamini and Hochberg, 1995] are:
enriched_GO=GO_wall$category[p.adjust(GO_wall$over_represented_pvalue, method="BH")<.05]
head(enriched_GO)
# character(0) ##########################################################################################################################################################
length(enriched_GO)
# [1] 0 ##########################################################################################################################################################


## Interaction effect (LRT)
# Constructing a gene vector for "goseq"
assayed_genes <- rownames(res_LRT)
de_genes <- rownames(sig_LRT)
genes_vector=as.integer(assayed_genes%in%de_genes)
names(genes_vector)=assayed_genes
table(genes_vector)

# To see which genome/gene identifier combinations are in the local database
supportedOrganisms() ########################## ERROR ##########################

supportedOrganisms()[supportedOrganisms()$Genome=="mm10",]

# Fitting the Probability Weighting Function (PWF)
pwf=nullp(genes_vector,"mm10","geneSymbol")
head(pwf)

# nullp plots the resulting fit, allowing verification of the goodness of fit before continuing the analysis
# The output of nullp contains all the data used to create the PWF, as well as the PWF itself.
# It is a data frame with 3 columns, named ”DEgenes”, ”bias.data” and ”pwf” with the rownames set to the gene names.
# Each row corresponds to a gene with the DEgenes column specifying if the gene is DE (1 for DE, 0 for not DE), the bias.data column giving the numeric value of the DE bias
# being accounted for (usually the gene length or number of counts) and the pwf column giving the genes value on the probability weighting function.

pdf(file = "/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_counts/goseq/pwf_Tg4510_interaction.pdf")
plotPWF(pwf)
dev.off()

# Using the Wallenius approximation
GO_wall=goseq(pwf,"mm10","geneSymbol")
# For 2543 genes, we could not find any categories. These genes will be excluded.
head(GO_wall)
# The resulting object is ordered by GO category over representation amongst DE genes.

# To identify categories significantly enriched/unenriched below some p-value cutoff, it is necessary to first apply some kind of multiple hypothesis testing correction.
# For example, GO categories over enriched using a .05 FDR cutoff [Benjamini and Hochberg, 1995] are:
enriched_GO=GO_wall$category[p.adjust(GO_wall$over_represented_pvalue, method="BH")<.05]
head(enriched_GO)
# [1] "GO:0016020" "GO:0071944" "GO:0005886" "GO:0002376" "GO:0006952"
# [6] "GO:0044425"

length(enriched_GO)
# 1042


BH_adj_GO <- p.adjust(GO_wall[,"over_represented_pvalue"], method="BH")
class(BH_adj_GO)
enriched_GO <- cbind(GO_wall, BH_adj_GO)
class(enriched_GO)
dim(enriched_GO)
head(enriched_GO)
enriched_GO <- enriched_GO[which(enriched_GO$BH_adj_GO<0.05),]
class(enriched_GO)
dim(enriched_GO)
head(enriched_GO)

write.csv(enriched_GO, file = "goseq/goseq_interaction.csv")

sessionInfo()

# R version 3.4.3 (2017-11-30)
# Platform: x86_64-redhat-linux-gnu (64-bit)
# Running under: CentOS Linux 7 (Core)
# GenomicFeatures_1.28.5
# BiocInstaller_1.28.0
# rtracklayer_1.36.6
# annotate_1.54.0
# XML_3.98-1.12
# org.Mm.eg.db_3.4.1
# goseq_1.30.0
# geneLenDataBase_1.12.0
# BiasedUrn_1.07
# GO.db_3.5.0
# nnotationDbi_1.40.0
# DESeq2_1.16.1