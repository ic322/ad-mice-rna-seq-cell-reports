#!/bin/bash

# Isabel Castanho I.S.Castanho@exeter.ac.uk

# Script to check expression of mouse and human (transgenic) MAPT - PART 2: get information

CSVFILE="test.csv"

while read line;
do
        if [[ "$line" =~ ^Processing\ sample.*$ ]]; then
                echo -n "${line}," | sed -e 's/Processing\ sample\ //g' >> $CSVFILE
        fi
        if [[ "$line" =~ ^Human$ ]]; then
                echo -n "${line}," >> $CSVFILE
        fi
        if [[ "$line" =~ ^Mouse$ ]]; then
                echo -n "${line}," >> $CSVFILE
        fi
        if [[ "$line" =~ ^.*\ reads\;\ of\ these\:$ ]]; then
                echo -n "${line}," | sed -e 's/\ reads\;\ of\ these\://g' >> $CSVFILE
        fi
        if [[ "$line" =~ ^.*\ aligned\ exactly\ 1\ time$ ]]; then
                echo -n "${line}," | sed -e 's/\ \(.*\)\ aligned\ exactly\ 1\ time//g' >> $CSVFILE
        fi
        if [[ "$line" =~ ^$ ]]; then
                echo "${line}" >> $CSVFILE
        fi
done < /mnt/data1/isabel/RNA-seq/analysis_isabel_new/mapt_transgene.out

sed -i '/^$/d' $CSVFILE