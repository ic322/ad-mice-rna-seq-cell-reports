#!/bin/bash

# Isabel Castanho I.S.Castanho@exeter.ac.uk

# Script to check expression of mouse and human (transgenic) MAPT - PART 1: alignment

# Input = trimmed fastq files
# Reference = MAPT 3UTR (~ 2kb)

# Mapping the reads
PATHS='/mnt/data1/isabel/RNA-seq/analysis_isabel_new/Tg4510_filtered/*/'

for path in $PATHS
do
	IFS='/' read -ra directoryNames <<< "$path"
	sampleName=${directoryNames[7]}

	echo "Processing sample $sampleName"
	
	dirpath=${path%/*} # ${path%/*} is the full path to the directory that has the files 

	F=$(find $dirpath | grep "fastq.filtered" | grep "R1") 
	R=$(find $dirpath | grep "fastq.filtered" | grep "R2")
	echo $F

echo "Human"
bowtie2 --very-fast -p 24 -x /mnt/data1/Aaron/isabel/bowtie2refs/human $F > test.sam
echo "Mouse"
bowtie2 --very-fast -p 24 -x /mnt/data1/Aaron/isabel/bowtie2refs/mouse $F > test.sam
	echo -e '\n'
done
